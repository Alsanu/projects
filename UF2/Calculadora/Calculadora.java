package Calculadora;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean salir = false;
		boolean num = false;
		int num1 = 0;
		int num2 = 0;
		
		do {
			System.out.println();
			System.out.println("Selecciona una de les següents opcions:");
			System.out.println("   1 - Obtenir numero/s");
			System.out.println("   + Suma");
			System.out.println("   - Resta");
			System.out.println("   * Multiplicació");
			System.out.println("   / Divisió");
			System.out.println("   2 - Veure Resultat");
			System.out.println("   0 - Sortir");
			System.out.println();
			System.out.print("Quina opció vols: ");
			String opc = sc.next();
			System.out.println();
			char opc2 = opc.charAt(0);
			switch(opc2) {
			case '1': 
				System.out.print("Introdueix 1 numero: ");
				num1 = sc.nextInt();
				break;
			case '+': 
				System.out.print("Amb quin numero vols sumar-ho: ");
				num1 = suma(num1, sc.nextInt()); break;
			case '-': 
				System.out.print("Amb quin numero vols restar-ho: ");
				num1 = resta(num1, sc.nextInt()); break;
			case '*': 
				System.out.print("Amb quin numero vols multiplicar-ho: ");
				num1 = mult(num1, sc.nextInt()); break;
			case '/': 
				System.out.print("Amb quin numero vols dividir-ho: ");
				System.out.println();
				num2 = sc.nextInt();
				if (num2 != 0) {
					System.out.print("Que vols obtenir: Residu (1) o Quocient (2): ");
					int op = sc.nextInt();
					if (op == 1) {
						num1 = res(num1, num2);
					} else if (op == 2) {
						num1 = div(num1, num2); 
					} else {
						System.out.println();
						System.out.println("Opcio no valida.");
					}
				} else {
					System.out.println("No es dividir entre 0");
				}
				break;
			case '2': System.out.println("El resultat es: " + num1); break;
			case '0': salir = true; break;
			}
		}while(!salir);
		
	}
	public static int suma(int a, int b) {
		return a+b;
	}
	public static int resta(int a, int b) {
		return a-b;
	}
	public static int mult(int a, int b) {
		return a*b;
	}
	public static int div(int a, int b) {
		return a/b;
	}
	public static int res(int a, int b) {
		return a%b;
	}
}
