package joel;

import java.util.Scanner;

public class AmbrosioTriangular {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt(); 
		for (int i = 0; i < casos; i++) {
			int filas = sc.nextInt();
			int ferreros = contarFerreros(filas);
			System.out.println(ferreros);
		}
	}

	public static int contarFerrerosBase(int fila) {
		int cont = 0;
		if (fila == 1) return 1;
		cont += fila;
		return cont + contarFerrerosBase(fila-1);
	}
	
	public static int contarFerreros(int fila) {
		int cont = 0;
		if (fila == 1) return 1;
		cont = contarFerrerosBase(fila);
		return cont + contarFerreros(fila-1);
	}
}
