package joel;

import java.util.Scanner;

public class TorretaKillJoy {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int j = 0; j < casos; j++) {
			int fc = sc.nextInt();
			int[][] matrix = new int[fc][fc];
			int tx = 0, cont = 0;
			for (int i = 0; i < fc; i++) {
				for (int x = 0; x < fc; x++) {
					matrix[i][x] = sc.nextInt();
					if (matrix[i][x] == 2) {
						tx = x;
					}
				}
			}
			for (int i = 0; i < fc; i++) {
				for (int x = 0; x < fc; x++) {
					if (x >= tx) {
						if (matrix[i][x] == 1) {
							 cont++;
						}
					}
				}
			}
			System.out.println("Spotted enemies: " + cont);
		}

	}

}
