package joel;

import java.util.Scanner;

public class Lego {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			num = lego(num);
			System.out.println(num);
		}
	}
	
	public static int lego(int n) {
		if (n == 0) {
			return n;
		}
		if (n == 2) {
			return n/2;
		}
		return (((n/2)*4)-4) + lego(n-2); 
	}
}
