package joel;

import java.util.Scanner;

public class Bitlles2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int bitlles = sc.nextInt();
			int files = numFiles(1, bitlles); 
			System.out.println(files);
		}
	}
	
	public static int numFiles(int fila, int num) {
		if (num<fila) return 0;
		return 1 + numFiles(fila+1, num-fila);
	}
}
