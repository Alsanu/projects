package joel;

import java.util.Scanner;

public class barrilesExplosivos {
    static int[][] matrix;
      static int fc;

      public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        fc = sc.nextInt();
        matrix = new int[fc][fc];

        for (int i = 0; i < fc; i++) {
          for (int j = 0; j < fc; j++) {
            matrix[i][j] = sc.nextInt();
          }
        }

        int f = sc.nextInt();
        int c = sc.nextInt();

        int nExp = explosion(f, c);
        if (nExp == 0) {
          System.out.println("Meh");
        } 
        else if (nExp == 1) {
          System.out.println("Boom");
        } 
        else if (nExp == 2) {
          System.out.println("Super Boom");
        } 
        else if (nExp == 3) {
          System.out.println("Super Ultra Boom");
        } 
        else {
          System.out.println("Explosion Nuclear");
        }
      }

      public static int explosion(int f, int c) {
        int cont = 0;
        if (f >= 0 && f < fc && c >= 0 && c < fc && matrix[f][c] == 1) {
          matrix[f][c] = 0;
          cont++;
          cont += explosion(f - 1, c);
          cont += explosion(f + 1, c);
          cont += explosion(f, c - 1);
          cont += explosion(f, c + 1);

          cont += explosion(f - 1, c - 1);
          cont += explosion(f + 1, c + 1);
          cont += explosion(f + 1, c - 1);
          cont += explosion(f - 1, c + 1);
        }
        return cont;
      }
    }
