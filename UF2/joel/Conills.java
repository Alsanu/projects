package joel;

import java.util.Scanner;

public class Conills {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();  //pisos de la torre de bombons quadrada
		
		for (int i=0;i<casos;i++) 
			System.out.println(fibonacci(sc.nextInt()));
	}

	static int fibonacci (int n) {
		if (n==0) return 0;
		if (n==1) return 1;		
		return fibonacci(n-1) + fibonacci(n-2);
	}	


}
