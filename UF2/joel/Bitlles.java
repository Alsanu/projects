package joel;

import java.util.Scanner;

public class Bitlles {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int files = sc.nextInt();
			int bitlles = numBitlles(files); 
			System.out.println(bitlles);
		}
	}
	
	public static int numBitlles(int num) {
		if (num <= 1) {
			return num;
		}
		return num + numBitlles(num-1);
	}
}
