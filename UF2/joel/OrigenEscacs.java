package joel;

import java.util.Scanner;

public class OrigenEscacs {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			long caselles = sc.nextInt();
			long blat = contBlad(caselles);
			System.out.println((blat*2)-1);
		}
	}

	public static long contBlad(long caselles) {
		if (caselles == 1) return caselles; 
		return 2 * contBlad(caselles - 1);
	}
}
