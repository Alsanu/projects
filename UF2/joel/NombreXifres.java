package joel;

import java.util.Scanner;

public class NombreXifres {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			long num = sc.nextInt();
			long xifres = contarXifres(num);
			System.out.println(xifres);
		}

	}
	
	public static long contarXifres(long num) {
		long cont = 1;
		if (num <= 9) return cont;
		num = num / 10;
		return cont + contarXifres(num);
	}
}
