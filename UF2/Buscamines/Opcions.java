package Buscamines;

import java.util.ArrayList; 
import java.util.Scanner;

public class Opcions {

	public static Scanner sc = new Scanner(System.in);
	
	public static void def(boolean definit, String[] jugador, ArrayList<String> jAUX, ArrayList<Integer> punts) {
		if (!definit) {
			crear(jugador, jAUX, punts);
			definit = true;
		}else {
			System.out.println("Jugadors disponibles");
			JugadorsDisponibles(jAUX);
			opcionsPerDefinir(jugador, jAUX, punts);
		}
		System.out.println();
	}
	
	public static void JugadorsDisponibles(ArrayList<String> jAUX) {
		System.out.println("-----------------------------------------------------------");
		for (int i = 0; i < jAUX.size(); i++) {
			System.out.print(jAUX.get(i) + "  |  ");
		}
		System.out.println();
		System.out.println("-----------------------------------------------------------");
		System.out.println();
	}
	
	public static void crear(String[] jugador, ArrayList<String> jAUX, ArrayList<Integer> punts) {
		System.out.print("Nom: ");
		String nom = sc.next().toUpperCase();
		if (!jAUX.contains(nom)) {
			jugador[0] = nom; jAUX.add(jugador[0]);
			punts.add(0);
		}else {
			System.out.println("Aquest jugador ja existeix");
		}
	}
	
	public static void canviar(String[] jugador, ArrayList<String> jAUX, ArrayList<Integer> punts) {
		System.out.print("Nom: ");
		String nom = sc.next().toUpperCase();
		if (jAUX.contains(nom)) {
			int pos = buscarJugador(jAUX, nom);
			jugador[0] = jAUX.get(pos);
		}else {
			System.out.print("Aquest jugador no existeix, vols crear-lo (SI) | (NO): ");
			String opc = sc.next().toUpperCase();
			if (opc.equals("SI")) {
				jugador[0] = nom; jAUX.add(jugador[0]);
				punts.add(0);
			}else {
				System.out.println("No s'ha realitzat cap canvi");
			}
		}
	}
	
	public static int buscarJugador(ArrayList<String> jAUX, String nom) {
		for (int i = 0; i < jAUX.size(); i++) {
			if (jAUX.get(i).equals(nom)) {
				return i;
			}
		}
		return 0;
	}
	
	public static void opcionsPerDefinir(String[] jugador, ArrayList<String> jAUX, ArrayList<Integer> punts) {
		System.out.println(" - Opcions -");
		System.out.println("(0) Cancelar");
		System.out.println("(1) Crear un nou jugador");
		System.out.println("(2) Utilitzar un jugador existents");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
		int opc = sc.nextInt();
		System.out.println();
		switch (opc) {
		case 1: crear(jugador, jAUX, punts); break;
		case 2: canviar(jugador, jAUX, punts) ; break;
		case 0: 
		default: System.out.println("Has sortit al menu inicial");
		}
	}
	
	public static void dades(String[] jugador, ArrayList<String> jAUX, ArrayList<Integer> punts) {
		String nom = jugador[0];
		int pos = buscarJugador(jAUX, nom);
		System.out.println("------------------------------");
		System.out.println("Nom:          " + jugador[0]);
		System.out.println("Victories:    " + punts.get(pos));
		System.out.println("------------------------------");
		System.out.println();
	}
	
	public static char[][] especificacions(int[] nfcm) {
		char[][] taulersecret = new char[0][0];
		System.out.println("------------------------------");
		System.out.println(" (0) Cancelar");
		System.out.println(" (1) Principiant   (Tauler 8x8, 10 mines)");
		System.out.println(" (2) Intermedi     (Tauler 16x16, 40 mines)");
		System.out.println(" (3) Expert        (Tauler 16x30, 99 mines)");
		System.out.println(" (4) Personalizat");
		System.out.print("Selecciona el nivell del joc: "); nfcm[0] = sc.nextInt();
		System.out.println();
		switch (nfcm[0]) {
		case 1: nfcm[1] = 9; nfcm[2] = 9; nfcm[3] = 10 ; break;
		case 2: nfcm[1] = 17; nfcm[2] = 17; nfcm[3] = 40; break;
		case 3: nfcm[1] = 17; nfcm[2] = 30; nfcm[3] = 99; break;
		case 4: System.out.print("Numero de files: "); nfcm[1] = sc.nextInt() + 1; 
		System.out.print("Numero de columnes: "); nfcm[2] = sc.nextInt() + 1; 
		System.out.print("Numero de mines: "); nfcm[3] = sc.nextInt(); break;
		case 0: 
		default: System.out.println("Has sortit al menu inicial - ");
		} 
		if (nfcm[0] > 0 && nfcm[0] < 5) {
			taulersecret = new char[nfcm[1]][nfcm[2]];
			crear(taulersecret, nfcm);
			return taulersecret; 
		}
		return taulersecret;
	}
	
	public static void crear(char[][]taulersecret, int[] nfcm) {
		for (int i = 0; i < nfcm[1]; i++) {
			for (int x = 0; x < nfcm[2]; x++) {
				if (x == 0) {
					taulersecret[i][x] = (char)(i+'0');
				}else if (i == 0) {
					taulersecret[i][x] = (char)(x+'0');
				}else {
					taulersecret[i][x] = '_';
				}
			}
		}
	}

}
