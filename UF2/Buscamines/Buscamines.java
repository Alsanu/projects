package Buscamines;

import java.util.ArrayList;
import java.util.Scanner;

public class Buscamines {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean definit = false;
		boolean sortir = false;
		String[] jugador = new String[1];
		ArrayList<String> jAUX = new ArrayList<String>();
		ArrayList<Integer> punts = new ArrayList<Integer>();
		char[][] tauler = null;
		char[][] taulersecret = null;
		int[] nfcm = new int[4]; // nivells files columnes mines
		
		do {
			if (!definit) {
				Inici();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Help.Ajuda(); break;
				case 2: Opcions.def(definit, jugador, jAUX, punts); definit = true; break;
				case 0: sortir = true; break; 
				default: System.out.println(" - Opcio no valida -");
				}
			}else {
				Principal();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Help.Ajuda();
				
				case 2: Opcions.def(definit, jugador, jAUX, punts); break;
				
				case 3: taulersecret = Opcions.especificacions(nfcm); 
				tauler = taulersecret; Joc.veureTauler(tauler, nfcm); break;
				
				case 4: Joc.FirstMove(taulersecret, taulersecret, nfcm); break;
				
				case 5: Opcions.dades(jugador, jAUX, punts); break;
				
				case 0: sortir = true; break;
				default: System.out.println(" - Opcio no valida -");
				}
			}
		}while(!sortir);
		sc.close();
	}
	
	public static void Inici() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
	public static void Principal() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	3 - Especificacions de la Partida");
		System.out.println("	4 - Jugar Partida");
		System.out.println("	5 - Dades Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}

}
