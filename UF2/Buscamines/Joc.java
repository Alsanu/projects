package Buscamines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Joc {

	public static Scanner sc = new Scanner(System.in);
	public static char MINA = 'X';
	public static char BANDERA = '/';
	public static char TAPAT = '_';
	
	public static void FirstMove(char[][] taulersecret, char[][] tauler, int[] nfcm) {
		int f = 0; int c = 0;
		veureTauler(tauler, nfcm);
		boolean valid = false;
		while (!valid) {
			System.out.print("Fila: "); f = sc.nextInt();
			if (f < 1 || f > nfcm[1]) {
				System.out.println("Coordenada no valida, proba ha posar una nova");
			}else {
				valid = true;
			}
		}
		valid = false;
		while (!valid) {
			System.out.print("Columna: "); c = sc.nextInt();
			if (c < 1 || c > nfcm[2]) {
				System.out.println("Coordenada no valida, proba ha posar una nova");
			}else {
				valid = true;
			}
		}
		generar(taulersecret, tauler, nfcm, f, c);
	}
	
	public static void generar(char[][] taulersecret, char[][] tauler, int[] nfcm, int f, int c) {
		ArrayList<Character> minas = new ArrayList<Character>();
		for (int i = 0; i < (nfcm[1]-1)*(nfcm[2]-1); i++) {
			if (i < nfcm[3]) {
				minas.add(MINA); 
			}else {
				minas.add(TAPAT);
			}
		}
		int max = minas.size()-1;
		Collections.shuffle(minas);
		int cont = 0;
		for (int i = 1; i < nfcm[1]; i++) {
			for (int x = 1; x < nfcm[2]; x++) {
				if (f == i && c == x) {
					if (cont <= nfcm[3]) {
						minas.set(max, MINA);
					}
				}
				taulersecret[i][x] = minas.get(cont);
				cont++;
			}
		}
		revelar(taulersecret, tauler, nfcm, f, c);
		veureTauler(tauler, nfcm);
	}
	
	public static int contar(char[][] taulersecret, char[][] tauler, int[] nfcm, int f, int c) {
		int cont = 0;
		for (int i = f-1; i < 3; i++) {
			for (int x = c-1; x < 3; x++) {
				if (i > 0 && i < nfcm[1] && x > 0 && x < nfcm[2]) {
					if (taulersecret[i][x] == MINA) {
						cont++;
					}
				}
			}
		}
		return cont;
	}
	
	public static void revelar(char[][] taulersecret, char[][] tauler, int[] nfcm, int f, int c) {
		tauler[f][c] = (char) (contar(taulersecret, tauler, nfcm, f, c)+'0');
		
		if (tauler[f][c] == 0) {
			for (int i = f-1; i < 3; i++) {
				for (int x = c-1; x < 3; x++) {
					if (i > 0 && i < nfcm[1] && x > 0 && x < nfcm[2]) {
						revelar(taulersecret, tauler, nfcm, i, x); 
					}
				}
			}
		}
		return;
	}
	
	public static void veureTauler(char[][] tauler, int[] nfcm) {
		for (int i = 0; i < nfcm[1]; i++) {
			for (int x = 0; x <  nfcm[2]; x++) {
					System.out.print(tauler[i][x] + "  ");
			}
			System.out.println();
			System.out.println();
		}
	}
}
