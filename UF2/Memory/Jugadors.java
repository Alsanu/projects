package Memory;

import java.util.Scanner;

public class Jugadors {

	public static Scanner sc = new Scanner(System.in);
	
	public static void def(boolean definit, String[] jugadors, int[] victories, int[] derrotes,  int[] punts, boolean[] bot, boolean[] inteligent) {
		int j1 = 0;
		int j2 = 1;
		if (!definit) {
			defj(j1, jugadors, victories, derrotes, punts, bot, inteligent); System.out.println();
			defj(j2, jugadors, victories, derrotes, punts, bot, inteligent);
		}else {
			seleccio(jugadors);
			int opc = sc.nextInt();
			sc.nextLine();
			System.out.println();
			switch (opc) {
			case 1: defj(j1, jugadors, victories, derrotes, punts, bot, inteligent); break;
			case 2: defj(j2, jugadors, victories, derrotes, punts, bot, inteligent); break;
			case 3: defj(j1, jugadors, victories, derrotes, punts, bot, inteligent); defj(j2, jugadors, victories, derrotes, punts, bot, inteligent); break;
			default: System.out.println(" - Opcio no valida -");
			}
		}
		System.out.println();
	}
	
	public static void defj(int j, String[] jugadors, int[] victories, int[] derrotes,  int[] punts, boolean[] bot, boolean[] inteligent) {
		System.out.println(" - Jugador " + (j+1));
		System.out.print("Nom: ");
		jugadors[j] = sc.nextLine();
		victories[j] = 0;
		derrotes[j] = 0;
		System.out.print("Bot ( SI | NO ): ");
		String bt = sc.next().toUpperCase();
		if (bt.equals("SI")) {
			bot[j] = true;
			System.out.print("Inteligent ( SI | NO ): ");
			bt = sc.next().toUpperCase();
			sc.nextLine();
			if (bt.equals("SI")) {
				inteligent[j] = true;
			}else {
				inteligent[j] = false;
			}
		}else {
			sc.nextLine();
			bot[j] = false; inteligent[j] = false;
		}
	}
	
	
	public static void seleccio(String[] jugadors) {
		System.out.println(" - Opcions:");
		System.out.println("(Si ja tenies jugadors creats, al crear uns nous s'elimaran els anteriors)");
		System.out.println("(1) Crear jugador: " + jugadors[0]);
		System.out.println("(2) Crear jugador: " + jugadors[1]);
		System.out.println("(3) Crear els dos jugadors");
		System.out.print("Quin jugador vols crear: ");
	}
	
	public static void dades(String[] jugadors, int[] victories, int[] derrotes,  int[] punts, boolean[] bot, boolean[] inteligent) {
		System.out.println("------------------------------");
		System.out.println("  - Jugador 1 -");
		System.out.println("Nom:          " + jugadors[0]);
		System.out.println("Victories:    " + victories[0]);
		System.out.println("Derrotes:     " + derrotes[0]);
		if (bot[0]) {
			if (inteligent[0]) {
				System.out.println("Bot:          Inteligent" );
			}else {
				System.out.println("Bot:          Random" );
			}
		}
		
		System.out.println();
		System.out.println("  - Jugador 2 -");
		System.out.println("Nom:          " + jugadors[1]);
		System.out.println("Victories:    " + victories[1]);
		System.out.println("Derrotes:     " + derrotes[1]);
		if (bot[1]) {
			if (inteligent[1]) {
				System.out.println("Bot:          Inteligent" );
			}else {
				System.out.println("Bot:          Random" );
			}
		}
		System.out.println("------------------------------");
		System.out.println();
	}
}
