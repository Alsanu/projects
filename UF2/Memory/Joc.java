package Memory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Joc {
	
	static Scanner sc = new Scanner(System.in);
	static Random rm = new Random();
	final static char TAPAT = '·';
	final static char AUX = 'O';
	
	public static void jugar(String[] jugadors, int[] victories, int[] derrotes, int[]punts, boolean[] bot, boolean[] inteligent) {
		char[][] memory = new char[4][4];
		char[][] codificat = new char[5][5];
		char[][] taulerAux = new char[5][5];
		int torn = rm.nextInt(2);
		tauler(memory); secret(codificat);
		secret(taulerAux); veuretauler(codificat);
		if (torn == 0) {
			System.out.println("Comença el jugador 1");
			game(torn, jugadors, punts, memory, codificat, bot, inteligent, taulerAux);
		}else {
			System.out.println(" - Comença el jugador 2");
			game(torn, jugadors, punts, memory, codificat, bot, inteligent, taulerAux);
		}
		guanyador(jugadors, victories, derrotes, punts);
		System.out.println();
	}
	
	
	
	public static void guanyador(String[] jugadors, int[] victories, int[] derrotes, int[]punts) {
		System.out.println();
		System.out.println("El jugador " + jugadors[0] + " te " + punts[0] + " punts");
		System.out.println("El jugador " + jugadors[1] + " te " + punts[1] + " punts");
		System.out.println();
		if (punts[0] > punts[1]) {
			System.out.println("¡Ha guanyat el jugador: " + jugadors[0] + "!");
			victories[0]++;
			derrotes[1]++;
		}else if (punts[0] < punts[1]) {
			System.out.println("¡Ha guanyat el jugador: " + jugadors[1] + "!");
			victories[1]++;
			derrotes[0]++;
		}else {
			System.out.println("¡Heu empatat!");
		}
	}
	
	public static void game(int torn, String[] jugadors, int[] punts, char[][] memory, char[][] codificat, boolean[] bot, boolean[] inteligent, char[][] taulerAux) {
		boolean fi = false;
		while (!fi) {
			if (torn % 2 == 0) {
				boolean encert = true;
				while (encert) {
					System.out.println();
					System.out.println(" - " + jugadors[0]);
					if (bot[0]) {
						if (inteligent[0]) {
							encert = demanarInteligent(memory, codificat, taulerAux);
						}else {
							encert = demanarBot(memory, codificat, taulerAux);
						}
					}else {
						encert = demanarJugador(memory, codificat, taulerAux);
					}
					if (encert) {
						punts[0]++;
						fi = acabar(codificat);
					}
					if (fi) {
						encert = false;
					}
				}
			}else {
				boolean encert = true;
				while (encert) {
					System.out.println();
					System.out.println(" - " + jugadors[1]);
					if (bot[1]) {
						if (inteligent[1]) {
							encert = demanarInteligent(memory, codificat, taulerAux);
						}else {
							encert = demanarBot(memory, codificat, taulerAux);
						}
					}else {
						encert = demanarJugador(memory, codificat, taulerAux);
					}
					if (encert) {
						punts[1]++;
						fi = acabar(codificat);
					}
					if (fi) {
						encert = false;
					}
				}
			}
			torn++;
		}
	}
	

	public static boolean acabar(char[][] codificat) {
		for (int i = 0; i < 5; i++) {
			for (int x = 0; x < 5; x++) {
				if (codificat[i][x] == TAPAT) {
					return false;
				}
			}
		}
		return true;
	}
	
	/*public static void Timer() {
		Timer timer = new Timer();
		
		TimerTask netejar = new TimerTask() {
			@Override
			public void run() {
				limpiar();
			}
		};
		timer.schedule(netejar, 3000);;
	}
	
	public static void limpiar() {
		
		for (int i = 0; i < 30; i++) {
			System.out.println();
		}
	}*/
	
	public static boolean demanarJugador(char[][] memory, char[][] codificat, char[][] taulerAux) {
		boolean encert = false;
		boolean valid = false;
		while (!valid) {
			boolean valid2 = false; 
			System.out.println("Primera Figura");
			System.out.print("PosY: "); int posY1 = sc.nextInt();
			System.out.print("PosX: "); int posX1 = sc.nextInt();
			if (posY1 > 0 && posY1 < 5 && posX1 > 0 && posX1 < 5 && codificat[posY1][posX1] == TAPAT) {
				taulerAux[posY1][posX1] = memory[posY1 - 1][posX1 - 1];
				valid = true;
				codificat[posY1][posX1] = memory[posY1 - 1][posX1 - 1];
				System.out.println();
				veuretauler(codificat);
				while (!valid2) {
					System.out.println("Segona Figura");
					System.out.print("PosY: "); int posY2 = sc.nextInt();
					System.out.print("PosX: "); int posX2 = sc.nextInt();
					if (posY2 > 0 && posY2 < 5 && posX2 > 0 && posX2 < 5 && codificat[posY2][posX2] == TAPAT ) {
						taulerAux[posY2][posX2] = memory[posY2 - 1][posX2 - 1];
						valid2 = true;
						codificat[posY2][posX2] = memory[posY2 - 1][posX2 - 1];
						System.out.println();
						veuretauler(codificat);
						encert = comprovar(codificat, posY1, posX1, posY2, posX2, taulerAux);
						if (encert) {
							return true;
						}else {
							return false;
						}
					}else if (taulerAux[posY2][posX2] != AUX) {
						codificat[posY2][posX2] = TAPAT;
						System.out.println();
						System.out.println(" - Posicio no valida - ");
						System.out.println();
					}else {
						System.out.println();
						System.out.println(" - Posicio no valida - ");
						System.out.println();
					}
				}
			}else {
				System.out.println();
				System.out.println(" - Posicio no valida - ");
				System.out.println();
			}
		}
		return false;
	}
	
	public static boolean demanarBot(char[][] memory, char[][] codificat, char[][] taulerAux) {
		boolean encert = false;
		boolean valid = false;
		while (!valid) {
			boolean valid2 = false; 
			int posY1 = rm.nextInt(1, 5);
			int posX1 = rm.nextInt(1, 5);
			if (posY1 > 0 && posY1 < 5 && posX1 > 0 && posX1 < 5 && codificat[posY1][posX1] == TAPAT) {
				System.out.println("Primera Figura");
				System.out.print("PosY: " + posY1 + " ");
				System.out.println("PosX: " + posX1);
				valid = true;
				codificat[posY1][posX1] = memory[posY1 - 1][posX1 - 1];
				taulerAux[posY1][posX1] = memory[posY1 - 1][posX1 - 1];
				System.out.println();
				veuretauler(codificat);
				while (!valid2) {
					int posY2 = rm.nextInt(1, 5);
					int posX2 = rm.nextInt(1, 5);
					if (posY2 > 0 && posY2 < 5 && posX2 > 0 && posX2 < 5 && codificat[posY2][posX2] == TAPAT && posY2 != posY1 && posX2 != posX1) {
						System.out.println("Segona Figura");
						System.out.print("PosY: " + posY2 + " ");
						System.out.print("PosX: " + posX2);
						valid2 = true;
						codificat[posY2][posX2] = memory[posY2 - 1][posX2 - 1];
						taulerAux[posY2][posX2] = memory[posY2 - 1][posX2 - 1];
						System.out.println();
						veuretauler(codificat);
						encert = comprovar(codificat, posY1, posX1, posY2, posX2, taulerAux);
						if (encert) {
							return true;
						}else {
							return false;
						}
					} else if (taulerAux[posY2][posX2] != AUX){
						codificat[posY2][posX2] = TAPAT;
					}
				}
			}
		}
		return false;
	}
	
	public static int[] omplirAux(char[][] taulerAux, int[] pos, int[] pos2, boolean f1) {
		if (f1) {
			for (int i = 1; i < 5; i++) {
				for (int x = 1; x < 5; x++) {
					if (taulerAux[i][x] != AUX && taulerAux[i][x] != TAPAT) {
						for (int i2 = 1; i2 < 5; i2++) {
							for (int x2 = 1; x2 < 5; x++) {
								if (taulerAux[i][x] == taulerAux[i2][x2]) {
									pos2[0] = i2; pos2[1] = x2; 
									return pos2;
								}
							}
						}
						for (int i2 = 1; i2 < 5; i2++) {
							for (int x2 = 1; x2 < 5; x++) {
								if (taulerAux[i][x] != taulerAux[i2][x2] && taulerAux[i2][x2] != AUX && taulerAux[i2][x2] != TAPAT) {
									pos2[0] = i2; pos2[1] = x2; 
									return pos2;
								}
							}
						}
					}
				}
			}
			boolean good = false;
			while (!good) {
				pos2[0] = rm.nextInt(1,5); pos2[1] = rm.nextInt(1,5);
				if (taulerAux[pos2[0]][pos2[1]] == TAPAT) {
					good = true;			
				}
			}
			return pos2;
		}else {
			for (int i = 1; i < 5; i++) {
				for (int x = 1; x < 5; x++) {
					if (taulerAux[i][x] != AUX && taulerAux[i][x] != TAPAT) {
						for (int i2 = 1; i2 < 5; i2++) {
							for (int x2 = 1; x2 < 5; x++) {
								if (taulerAux[i][x] == taulerAux[i2][x2]) {
									pos[0] = i; pos[1] = x;
									return pos;
								}
							}
						}
					}
				}
			}
			boolean good = false;
			while (!good) {
				pos[0] = rm.nextInt(1,5); pos[1] = rm.nextInt(1,5);
				if (taulerAux[pos[0]][pos[1]] == TAPAT) {
					good = true;
				}
			}
			return pos;
		}
	}
	
	public static boolean demanarInteligent(char[][] memory, char[][] codificat, char[][] taulerAux) {
		int[] pos = new int[2];
		int[] pos2 = new int[2];
		boolean encert = false;
		boolean f1 = false;
		pos = omplirAux(taulerAux, pos, pos2, f1); 
		if (pos[0] > 0 && pos[0] < 5 && pos[1] > 0 && pos[1] < 5 && codificat[pos[0]][pos[1]] == TAPAT) {
			System.out.println("Primera Figura");
			System.out.print("PosY: " + pos[0] + " ");
			System.out.println("PosX: " + pos[1]);
			taulerAux[pos[0]][pos[1]] = memory[pos[0] - 1][pos[1] - 1];
			codificat[pos[0]][pos[1]] = memory[pos[0] - 1][pos[1] - 1];
			System.out.println();
			veuretauler(codificat);
			f1 = true;
			pos2 = omplirAux(taulerAux, pos, pos2, f1);
			if (pos2[0] > 0 && pos2[0] < 5 && pos2[1] > 0 && pos2[1] < 5 && codificat[pos2[0]][pos2[1]] == TAPAT && pos2[0] != pos[0] && pos2[1] != pos[1]) {
				System.out.println("Segona Figura");
				System.out.print("PosY: " + pos2[0] + " ");
				System.out.println("PosX: " + pos2[1]);
				taulerAux[pos2[0]][pos2[1]] = memory[pos2[0] - 1][pos2[1] - 1];
				codificat[pos2[0]][pos2[1]] = memory[pos2[0] - 1][pos2[1] - 1];
				System.out.println();
				veuretauler(codificat);
				encert = comprovar(codificat, pos[0], pos[1], pos2[0], pos2[0], taulerAux);
				if (encert) {
					return true;
				}else {
					return false;
				}
			}else if (taulerAux[pos2[0]][pos[1]] != AUX){
				codificat[pos2[0]][pos2[1]] = TAPAT;
			}
			return false;
		}
		return false;
	}
	
	public static boolean comprovar(char[][] codificat, int posY1, int posX1, int posY2, int posX2, char[][] taulerAux) {
		if (codificat[posY1][posX1] == codificat[posY2][posX2]) {
			System.out.println(" !Has Encertat! ");
			taulerAux[posY1][posX1] = AUX;
			taulerAux[posY2][posX2] = AUX;
			return true;
		}
		codificat[posY1][posX1] = TAPAT;
		codificat[posY2][posX2] = TAPAT;
		System.out.println(" !Has Errat¡ ");
		return false;
	}
	
	public static void veuretauler(char[][] codificat) {
		for (int i = 0; i < 5; i++) {
			for (int x = 0; x < 5; x++) {
				System.out.print(codificat[i][x] + "  ");
			}
			System.out.println();
			System.out.println();
		}
	}
	
	public static void tauler(char[][] memory) {
		ArrayList<Character> simbols = new ArrayList<Character>();
		simbols.add('X'); simbols.add('X');
		simbols.add('#'); simbols.add('#');
		simbols.add('+'); simbols.add('+');
		simbols.add('€'); simbols.add('€');
		simbols.add('@'); simbols.add('@');
		simbols.add('%'); simbols.add('%');
		simbols.add('$'); simbols.add('$');
		simbols.add('&'); simbols.add('&');	
		Collections.shuffle(simbols);
		int cont = 0;
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 4; x++) {
				memory[i][x] = simbols.get(cont);
				cont++;
			}
		}
	}
	
	public static void secret(char[][] codificat) {
		int cont1 = 0;
		int cont2 = 1;
		for (int i = 0; i < 5; i++) {
			for (int x = 0; x < 5; x++) {
				if (i == 0) {
					codificat[i][x] = (char)(cont1+'0');
					cont1++;
				}else if (x == 0) {
					codificat[i][x] = (char)(cont2+'0');
					cont2++;
				}else {
					codificat[i][x] = TAPAT;
				}
			}
		}
	}
	
}
