package Memory;

import java.util.Scanner;

public class Memory {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean definit = false;
		boolean sortir = false;
		String[] jugadors = new String[2];
		int[] victories = new int[2];
		int[] derrotes = new int[2];
		int[] punts = new int[2];
		boolean[] bot = new boolean[2];
		boolean[] inteligent = new boolean[2];
		
		do {
			if (!definit) {
				Inici();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Help.Ajuda(); break;
				case 2: Jugadors.def(definit, jugadors, victories, derrotes, punts, bot, inteligent); definit = true; break;
				case 0: sortir = true; break; 
				default: System.out.println(" - Opcio no valida -");
				}
			}else {
				Principal();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Help.Ajuda();
				case 2: Jugadors.def(definit, jugadors, victories, derrotes, punts, bot, inteligent); break;
				case 3: Joc.jugar(jugadors, victories, derrotes, punts, bot, inteligent); break;
				case 4: Jugadors.dades(jugadors, victories, derrotes, punts, bot, inteligent); break;
				case 0: sortir = true; break;
				default: System.out.println(" - Opcio no valida -");
				}
			}
		}while(!sortir);
		sc.close();
	}
	
	public static void Inici() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
	public static void Principal() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	3 - Jugar Partida");
		System.out.println("	4 - Dades Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}

}
