package tresRatlla;

import java.util.Random;
import java.util.Scanner;

public class TresRatlla {
	public static Jugadors j1 = new Jugadors();
	public static Jugadors j2 = new Jugadors();
	static boolean torn;
	public static int guanyador;
	
	public static void main(String[] args) {
		Jugadors j1 = new Jugadors();
		Jugadors j2 = new Jugadors();
		Scanner sc = new Scanner(System.in);
		boolean sortir = false;
		boolean mip = false;
		do {
			if (!mip) {
				Menus.Inici();
				int opc = sc.nextInt();
				System.out.println();
				
				switch(opc) {
				case 1: Ajuda.Ajuda(); break;
				
				case 2: System.out.println(" - Jugador1 -"); Jugadors.omplirjugador(j1); 
				System.out.println(" - Jugador2 -"); Jugadors.omplirjugador(j2); mip = true; break;
				
				case 0: sortir = true; break;
				default: System.out.println("	- Opcio no valida -"); System.out.println();
				}
			} else {
				Menus.Principal();
				int opc = sc.nextInt();
				System.out.println();
				
				switch(opc) {
				case 1: Ajuda.Ajuda(); break;
				
				case 2: System.out.println(" - Jugador1 -"); Jugadors.omplirjugador(j1); 
				System.out.println(" - Jugador2 -"); Jugadors.omplirjugador(j2); break;
				
				case 3: Partida.Clear(); Partida.Tauler(); Partida.VeureTauler() ; Jugar(j1, j2); SumViDe(j1, j2); break;
				
				case 4: System.out.println(" - Jugador1 -"); Jugadors.DadesJugadors(j1); 
				System.out.println(" - Jugador2 -"); Jugadors.DadesJugadors(j2); break;
				
				case 0: sortir = true; break;
				default: System.out.println("	- Opcio no valida"); System.out.println();
				}
			}
			
		}while(!sortir);
	}
	
	public static void Jugar(Jugadors j1, Jugadors j2) {
		guanyador = 0;
		int cont = 0;
		Random rm = new Random();
		int ran = rm.nextInt(2);
		if (ran == 0) {
			System.out.println(" - Comença " + j1.nom);
			System.out.println();
			while (guanyador == 0 && cont < 9) {
				if (cont % 2 == 0) {
					System.out.println("- " + j1.nom); torn = true; 
					Player(j1); guanyador = Partida.comprovar();
				} else {
					System.out.println("- " + j2.nom); torn = false; 
					Player(j2); guanyador = Partida.comprovar();
				}
				cont++;
			}
		} else {
			System.out.println(" - Comença " + j2.nom);
			System.out.println();
			while (guanyador == 0 && cont < 9) {
				if (cont % 2 == 0) {
					System.out.println("- " + j2.nom); torn = false; 
					Player(j2); guanyador = Partida.comprovar();
				} else { 
					System.out.println("- " + j1.nom); torn = true; 
					Player(j1); guanyador = Partida.comprovar();
				}
				cont++;
			}	
		}
		Win(j1, j2);
	}
	
	public static void Win(Jugadors j1, Jugadors j2) {
		if (guanyador == 1) {
			System.out.println("Ha guanyat " + j1.nom);
			System.out.println();
		}else if (guanyador == 2) {
			System.out.println("Ha guanyat " + j2.nom);
			System.out.println();
		}else {
			System.out.println("Tables");
			System.out.println();
		}
	}
	
	public static void SumViDe(Jugadors j1, Jugadors j2) {
		if (guanyador == 1) {
			j1.Vi++;
			j2.De++;
		} 
		if (guanyador == 2) {
			j2.Vi++;
			j1.De++;
		}
	}
	
	public static void Player(Jugadors j1) {
		if (!j1.bot) {
			Partida.marcar();
		}else {
			Partida.Bot();
		}
	}
}
