package tresRatlla;

import java.util.Scanner;

public class Jugadors {
	public String nom; // nombre
	public int Vi; // numero victorias
	public int De; // numero derrotes
	public boolean bot; // El jugador sera persona o bot?
	public boolean nivell;	// Si es bot quin nivell de joc te?
	
	public static void omplirjugador(Jugadors j1) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Defineix el nom del jugador: "); j1.nom = sc.nextLine();
		j1.Vi = 0;
		j1.De = 0;
		System.out.print("Es un bot? (SI) / (NO): "); String bop = sc.next();
		if (bop.equals("SI")) {
			j1.bot = true;
			System.out.print("Vols que el bot sigui inteligent? (SI) / (NO): "); String nop = sc.next();
			if (nop.equals("SI")) {
				j1.nivell = true;
			}
		} else {
			j1.bot = false;
		}
		System.out.println();
	}
	
	public static void DadesJugadors(Jugadors j1) {
		System.out.println("Nom: " + j1.nom);
		System.out.println("Numero de Victories: " + j1.Vi);
		System.out.println("Numero de Derrotes: " + j1.De);
		System.out.println("Bot: " + j1.bot);
		if (j1.bot) {
			System.out.println("Inteligent: " + j1.nivell);
		}
		System.out.println();
	}
}
