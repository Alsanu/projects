package tresRatlla;

import java.util.Random;
import java.util.Scanner;

public class Partida {
	static char[][] matrix = new char[4][4];
	static int posX;
	static int posY;
	public static char simb;
	public static char simb2;
	
	public static void Clear() {
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 4; x++) {
				matrix[i][x] = '-';
			}
		}
	}
	
	public static void Tauler() {
		int cont1 = 0;
		int cont2 = 0;
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 4; x++) {
				if (i == 0) {
					char posX = (char) (cont1+'0');
					matrix[i][x] = posX;
					cont1++;
				}
				if (x == 0) {
					char posY = (char) (cont2+'0');
					matrix[i][x] = posY;
					cont2++;
				}
			}
		}
		System.out.println();
	}
	public static void VeureTauler() {
		for (int i = 0; i < 4; i++) {
			for (int x = 0; x < 4; x++) {
				System.out.print(matrix[i][x] + "   ");
			}
			System.out.println();
			System.out.println();
		}
	}
	
	public static int comprovar() {
		int jugador; // simbol a comprovar
		if (TresRatlla.torn) {
			jugador = 1;
			simb = 'X';
		} else {
			jugador = 2;
			simb = 'O';
		}
		if (matrix[posY][1] == matrix[posY][2] && matrix[posY][2] ==  matrix[posY][3]) {
			return jugador;
		}
		if (matrix[1][posX] ==  matrix[2][posX] && matrix[2][posX] == matrix[3][posX]) {
			return jugador;
		}
		if (matrix[1][1] == simb && matrix[2][2] == simb && matrix[3][3] == simb) {
			return jugador;
		}
		if (matrix[1][3] == simb && matrix[2][2] == simb && matrix[3][1] == simb) {
			return jugador;
		}
		return 0;
	}
	
	public static void marcar() {
		Scanner sc = new Scanner(System.in);
		boolean valid = false;
		while (!valid) {
			System.out.print("Posicion Y: "); posY = sc.nextInt();
			System.out.print("Posicion X: "); posX = sc.nextInt();
			if (matrix[posY][posX] == 'X' || matrix[posY][posX] == 'O' || posY <= 0 || posX <= 0 || posY > 3 || posX > 3) {
				System.out.println(" - Aquesta posico lla ha sigut seleccionada /n o es una posicio erronea \nen conseqüencia no es pot tornar a seleccionar - ");
			} else {
				valid = true;
			}
		}	
		if (TresRatlla.torn) {
			matrix[posY][posX] = 'X';
		} else {
			matrix[posY][posX] = 'O';
		}
		System.out.println();
		Partida.VeureTauler();
	}
	
	public static void Bot() {
		Random rm = new Random();
		Scanner sc = new Scanner(System.in);
		boolean valid = false;
		while (!valid) {
			posY = rm.nextInt(1, 4);
			posX = rm.nextInt(1, 4);
			if (matrix[posY][posX] != 'X' && matrix[posY][posX] != 'O') {
				valid = true;
				System.out.println("Posicion Y: " + posY); 
				System.out.println("Posicion X: " + posX);
			}
		}	
		if (TresRatlla.torn) {
			matrix[posY][posX] = 'X';
		} else {
			matrix[posY][posX] = 'O';
		}
		System.out.println();
		Partida.VeureTauler();
	}
	
	/*public static void Up() {
		Random rm = new Random();
		Scanner sc = new Scanner(System.in);
		boolean valid = false;
		while (!valid) {
			posY = 0; posX = 0;
			if (TresRatlla.torn) {
				if (matrix[2][2] == '-') {
					posY = 2; posX = 2;
				}else {
					simb = 'O';
					simb2 = 'X';
					posibilitat1();
					System.out.println("Pos1");
					if (posY == 0 && posX == 0) {
						posibilitat2();
						System.out.println("Pos2");
					}
					if (posY == 0 && posX == 0) {
						posY = rm.nextInt(1,4); posX = rm.nextInt(1,4);
						System.out.println("Random");
					}
				}
			}else {
				if (matrix[2][2] == '-') {
					posY = 2; posX = 2;
				}else {
					simb = 'X';
					simb2 = 'O';
					posibilitat1();
					System.out.println("Pos1");
					if (posY == 0 && posX == 0) {
						posibilitat2();
						System.out.println("Pos2");
					}
					if (posY == 0 && posX == 0) {
						posY = rm.nextInt(1,4); posX = rm.nextInt(1,4);
						System.out.println("Random");
					}
				}
			}
			
			System.out.println("Posicion Y: " + posY);
			System.out.println("Posicion X: " + posX);
			if (matrix[posY][posX] == 'X' || matrix[posY][posX] == 'O') {
				System.out.println(" - Aquesta posico lla ha sigut seleccionada \nen conseqüencia no es pot tornar a seleccionar - ");
			} else {
				valid = true;
			}
		}	
		if (TresRatlla.torn) {
			matrix[posY][posX] = 'X';
		} else {
			matrix[posY][posX] = 'O';
		}
		System.out.println();
		Partida.VeureTauler();
	}
	
	public static void posibilitat1() {
		columnes1();
		if (posY == 0 && posX == 0) {
			files1();
			if (posY == 0 && posX == 0) {
				diagonals1();
			}
		}
	}
	
	public static void posibilitat2() {
		columnes2();
		if (posY == 0 && posX == 0) {
			files2();
			if (posY == 0 && posX == 0) {
				diagonals2();
			}
		}
	}
	
	public static void columnes1() {
		if (matrix[1][1] == simb && matrix[1][2] == simb && matrix[1][3] == '-') {
			posY = 1; posX = 3;
		}else if (matrix[1][1] == simb2 && matrix[1][2] == '-' && matrix[1][3] == simb2) {
			posY = 1; posX = 2;
		}else if (matrix[1][1] == '-' && matrix[1][2] == simb2 && matrix[1][3] == simb2) {
			posY = 1; posX = 1;
		}else if (matrix[2][1] == simb2 && matrix[2][2] == simb2 && matrix[2][3] == '-') {
			posY = 2; posX = 3;
		}else if (matrix[2][1] == simb2 && matrix[2][2] == '-' && matrix[2][3] == simb2) {
			posY = 2; posX = 2;
		}else if (matrix[2][1] == '-' && matrix[2][2] == simb2 && matrix[2][3] == simb2) {
			posY = 2; posX = 1;
		}else if (matrix[3][1] == simb2 && matrix[3][2] == simb2 && matrix[3][3] == '-') {
			posY = 3; posX = 3;
		}else if (matrix[3][1] == simb2 && matrix[3][2] == '-' && matrix[3][3] == simb2) {
			posY = 3; posX = 2;
		}else if (matrix[3][1] == '-' && matrix[3][2] == simb2 && matrix[3][3] == simb2) {
			posY = 3; posX = 1;
		}
	}
	
	public static void columnes2() {	
		if (matrix[1][1] == simb && matrix[1][2] == simb && matrix[1][3] == '-') {
			posY = 1; posX = 3;
		}else if (matrix[1][1] == simb && matrix[1][2] == '-' && matrix[1][3] == simb) {
			posY = 1; posX = 2;
		}else if (matrix[1][1] == '-' && matrix[1][2] == simb && matrix[1][3] == simb) {
			posY = 1; posX = 1;
		}else if (matrix[2][1] == simb && matrix[2][2] == simb && matrix[2][3] == '-') {
			posY = 2; posX = 3;
		}else if (matrix[2][1] == simb && matrix[2][2] == '-' && matrix[2][3] == simb) {
			posY = 2; posX = 2;
		}else if (matrix[2][1] == '-' && matrix[2][2] == simb && matrix[2][3] == simb) {
			posY = 2; posX = 1;
		}else if (matrix[3][1] == simb && matrix[3][2] == simb && matrix[3][3] == '-') {
			posY = 3; posX = 3;
		}else if (matrix[3][1] == simb && matrix[3][2] == '-' && matrix[3][3] == simb) {
			posY = 3; posX = 2;
		}else if (matrix[3][1] == '-' && matrix[3][2] == simb && matrix[3][3] == simb) {
			posY = 3; posX = 1;
		}
		
	}
	
	public static void files1() {
		if (matrix[1][1] == simb2 && matrix[2][1] == simb2 && matrix[3][1] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == simb2 && matrix[2][1] == '-' && matrix[3][1] == simb2) {
			posY = 2; posX = 1;
		}else if (matrix[1][1] == '-' && matrix[2][1] == simb2 && matrix[3][1] == simb2) {
			posY = 1; posX = 1;
		}else if (matrix[1][2] == simb2 && matrix[2][2] == simb2 && matrix[3][2] == '-') {
			posY = 3; posX = 2;
		}else if (matrix[1][2] == simb2 && matrix[2][2] == '-' && matrix[3][2] == simb2) {
			posY = 2; posX = 2;
		}else if (matrix[1][2] == '-' && matrix[2][2] == simb2 && matrix[3][2] == simb2) {
			posY = 1; posX = 2;
		}else if (matrix[1][3] == simb2 && matrix[2][3] == simb2 && matrix[3][3] == '-') {
			posY = 3; posX = 3;
		}else if (matrix[1][3] == simb2 && matrix[2][3] == '-' && matrix[3][3] == simb2) {
			posY = 2; posX = 3;
		}else if (matrix[1][3] == '-' && matrix[2][3] == simb2 && matrix[3][3] == simb2) {
			posY = 1; posX = 3;
		}
	}
	
	public static void files2() {
		if (matrix[1][1] == simb && matrix[2][1] == simb && matrix[3][1] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == simb && matrix[2][1] == '-' && matrix[3][1] == simb) {
			posY = 2; posX = 1;
		}else if (matrix[1][1] == '-' && matrix[2][1] == simb && matrix[3][1] == simb) {
			posY = 1; posX = 1;
		}else if (matrix[1][2] == simb && matrix[2][2] == simb && matrix[3][2] == '-') {
			posY = 3; posX = 2;
		}else if (matrix[1][2] == simb && matrix[2][2] == '-' && matrix[3][2] == simb) {
			posY = 2; posX = 2;
		}else if (matrix[1][2] == '-' && matrix[2][2] == simb && matrix[3][2] == simb) {
			posY = 1; posX = 2;
		}else if (matrix[1][3] == simb && matrix[2][3] == simb && matrix[3][3] == '-') {
			posY = 3; posX = 3;
		}else if (matrix[1][3] == simb && matrix[2][3] == '-' && matrix[3][3] == simb) {
			posY = 2; posX = 3;
		}else if (matrix[1][3] == '-' && matrix[2][3] == simb && matrix[3][3] == simb) {
			posY = 1; posX = 3;
		}
		
	}
	
	public static void diagonals1() {
		if (matrix[1][1] == simb2 && matrix[2][2] == simb2 && matrix[3][3] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == simb2 && matrix[2][2] == '-' && matrix[3][3] == simb2) {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == '-' && matrix[2][2] == simb2 && matrix[3][3] == simb2) {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == simb2 && matrix[2][2] == simb2 && matrix[3][1] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == simb2 && matrix[2][2] == '-' && matrix[3][1] == simb2) {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == '-' && matrix[2][2] == simb2 && matrix[3][1] == simb2) {
			posY = 3; posX = 1;
		}
	}
	
	public static void diagonals2() {
		if (matrix[1][1] == simb && matrix[2][2] == simb && matrix[3][3] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == simb && matrix[2][2] == '-' && matrix[3][3] == simb) {
			posY = 3; posX = 1;
		}else if (matrix[1][1] == '-' && matrix[2][2] == simb && matrix[3][3] == simb) {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == simb && matrix[2][2] == simb && matrix[3][1] == '-') {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == simb && matrix[2][2] == '-' && matrix[3][1] == simb) {
			posY = 3; posX = 1;
		}else if (matrix[1][3] == '-' && matrix[2][2] == simb && matrix[3][1] == simb) {
			posY = 3; posX = 1;
		}
	}*/
}
