package tresRatlla;

public class Ajuda {
	
	public static void Ajuda() {
		System.out.println("Explicacio del Joc: ");
		System.out.println(" - El tres en ratlla és un joc de tauler, es jugar amb dos jugadors,\n "
				+ "per torns, on guanyara el primer en arribar a posar tres peces sobre\n "
				+ "el tauler de manera que siguen en línia recta.");
		System.out.println("Explicacio Opcions del Joc: ");
		System.out.println(" - L'opcio 2 (Definir Jugadors) serveix per definir "
				+ "les caracteristiques dels jugadors");
		System.out.println(" - L'opcio 3 (Jugar Partida) inicialitzara una partida "
				+ "amb les dades dels jugadors definits anteriorments");
		System.out.println(" - L'opcio 4 (Dades Jugadors) s'utilitza per veure les dades dels jugadors");
		System.out.println();
	}
}
