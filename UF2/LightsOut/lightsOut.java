package LightsOut;

import java.util.Scanner;

public class lightsOut {
	
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		boolean sortir = false;
		boolean definit = false;
		int[][] tauler = new int[0][0];
		int[] mida = new int[1];
		
		do {
			if (!definit) {
				Inici();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: tauler = configuracio(mida); definit = true; break; // Configuracio
				case 0: sortir = true; break; 
				default: System.out.println(" - Opcio no valida -");
				}
			}else {
				Principal();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				
				case 1: tauler = configuracio(mida); break; // Configuracio
				
				case 2: joc.inicialitzar(tauler, mida); joc.veureTauler(tauler, mida); joc.jugar(tauler, mida); break; // Jugar
				
				case 0: sortir = true; break;
				default: System.out.println(" - Opcio no valida -");
				
				}
			}
		}while(!sortir);
		sc.close();
	}

	public static void Inici() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Configuració");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
	public static void Principal() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Configuració");
		System.out.println("	2 - Jugar");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
	public static int[][] configuracio(int[] mida) {
		boolean valid = false;
		mida[0] = 0;
		System.out.println("(Jugarem a un tauler cuadrat, mida maxima: 20)");
		while (!valid) {
			System.out.print("Quina sera la mida del tauler: "); mida[0] = sc.nextInt()+1;
			System.out.println();
			if (mida[0] <= 0 || mida[0] > 21) { // MODIFICAT EL 20 PER 21
				System.out.println("Aquesta mida no es correcte, torna-ho a intentar");
			}else {
				valid = true;
			}
		}
		int[][] tauler = new int[mida[0]][mida[0]];
		
		return tauler;
	}
	
	
}
