package LightsOut;

import java.util.ArrayList;
import java.util.Scanner;

public class Act3 {

	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		boolean sortir = false;
		float notaFinal = 0;
		ArrayList<Integer> punts = new ArrayList<Integer>();
		do {
			System.out.println("OPCIONS DEL PROGRAMA:");
			System.out.println("	1 - Obtenir Punts");
			System.out.println("	2 - Visualitzar Punts");
			System.out.println("	3 - Obtenir Nota Final");
			System.out.println("	0 - Sortir");
			System.out.println();
			System.out.print("Seleciona una opcio: ");
			int opc = sc.nextInt();
			System.out.println();
			switch(opc) {
			
			case 1: Obtenir(punts); break;
			
			case 2: Visualitzar(punts); break;
			
			case 3: notaFinal = NotaFinal(punts); System.out.println("Nota Final: " + notaFinal); System.out.println(); break;
			
			case 0: sortir = true; break;
			default: System.out.println(" - Opcio no valida -");
			}
			
		}while(!sortir);

	}

	public static void Obtenir(ArrayList<Integer> punts) {
		for (int i = 0; i < 5; i++) {
			punts.add(sc.nextInt());
		}
	}
	
	public static void Visualitzar(ArrayList<Integer> punts) {
		for (int i = 0; i < 5; i++) {
			System.out.print(punts.get(i) + " ");
		}
		System.out.println();
	}
	
	public static float NotaFinal(ArrayList<Integer> punts) {
		float nota = 0;
		int max = 0, min = 0;
		for (int i = 0; i < 5; i++) {
			if (punts.get(i) > max) {
				max = punts.get(i);
			}
			if (punts.get(i) < min) {
				min = punts.get(i);
			}
		}
		for (int i = 0; i < 5; i++) {
			if (punts.get(i) == min || punts.get(i) == max) {
				punts.set(i, 0);
			}
			nota += punts.get(i);
		}
		nota = nota / 3;
		return nota;
	}
}
