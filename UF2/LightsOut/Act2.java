package LightsOut;

import java.util.Scanner;

public class Act2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			int pasos = Recursivitat(num);
			System.out.println(pasos-1);
		}
	}

	public static int Recursivitat(int num) {
		int cont = 1;
		if (num == 1) return 1;
		if (num%2 == 0) return cont + Recursivitat(num/2);
		return cont + Recursivitat((num*3)+1);
	}
}
