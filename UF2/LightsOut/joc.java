package LightsOut;

import java.util.Random;
import java.util.Scanner;

public class joc {
	
	public static Scanner sc = new Scanner(System.in);

	public static void inicialitzar(int[][] tauler, int[] mida) {
		Random rm = new Random();
		for (int i = 0; i < mida[0]; i++) {
			for (int x = 0; x < mida[0]; x++) {
				if (x == 0) {
					tauler[i][x] = i;
				}else if (i == 0) {
					tauler[i][x] = x;
				}else {
					tauler[i][x] = rm.nextInt(2);
				}
			}
		}
	}
	
	public static void veureTauler(int[][] tauler, int[] mida) {
		for (int i = 0; i < mida[0]; i++) {
			for (int x = 0; x < mida[0]; x++) {
				System.out.print(tauler[i][x] + " ");
				if (x > 9 && i > 0) { // HE AFEGIT AQUEST IF PER MILLORAR L'ESTETICA
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void jugar(int[][] tauler, int[] mida) {
		boolean fi = false;
		int fila = 0, columna = 0;
		while (!fi) {
			System.out.print("Fila: "); fila = sc.nextInt();
			if (fila <= 0 || fila >= mida[0]) {
				System.out.println("Aquesta fila no es correcte, torna-ho a intentar");
			}else {
				while (!fi) {
					System.out.print("Columna: "); columna = sc.nextInt();
					if (columna <= 0 || columna >= mida[0]) {
						System.out.println("Aquesta columna no es correcte, torna-ho a intentar");
					}else {
						fi = true;
					}
				}
			}
			marcar(tauler, fila, columna, mida);
			fi = comprovar(tauler, mida);
			veureTauler(tauler, mida);
		}
		System.out.println("    ¡¡¡Has Guanyat!!!");
		System.out.println(); //LINEA AFEGIDA
	}
	
	public static void marcar(int[][] tauler, int f, int c, int[] mida) {
		if (tauler[f][c] == 0) {
			tauler[f][c] = 1;
		}else if (tauler[f][c] == 1) {
			tauler[f][c] = 0;
		}
		
		if (f > 1) {
			if (tauler[f-1][c] == 0) {
				tauler[f-1][c] = 1;
			}else if (tauler[f-1][c] == 1) {
				tauler[f-1][c] = 0;
			}
		}
		
		if (c > 1) {
			if (tauler[f][c-1] == 0) {
				tauler[f][c-1] = 1;
			}else if (tauler[f][c-1] == 1) {
				tauler[f][c-1] = 0;
			}
		}
		
		if (f+1 < mida[0]) {
			if (tauler[f+1][c] == 0) {
				tauler[f+1][c] = 1;
			}else if (tauler[f+1][c] == 1) {
				tauler[f+1][c] = 0;
			}
		}
		
		if (c+1 < mida[0]) {
			if (tauler[f][c+1] == 0) {
				tauler[f][c+1] = 1;
			}else if (tauler[f][c+1] == 1) {
				tauler[f][c+1] = 0;
			}
		}
	}
	
	public static boolean comprovar(int[][] tauler, int[] mida) {
		boolean fi = true;
		int aux = tauler[1][1];
		for (int i = 1; i < mida[0]; i++) {
			for (int x = 1; x < mida[0]; x++) {
				if (tauler[i][x] != aux) {
					fi = false;
				}
			}
		}
		return fi;
	}
	
}
