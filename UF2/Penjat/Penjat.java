package Penjat;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Penjat {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean sortir = false;
		boolean definit = false;
		ArrayList<String> jugador = new ArrayList<String>();
		ArrayList<Integer> Victories = new ArrayList<Integer>();
		ArrayList<Integer> Derrotes = new ArrayList<Integer>();
		
		do {
			if (!definit) {
				Inici();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Ajuda.Ajuda(); break;
				case 2: Jugador.def(jugador, Victories, Derrotes);definit = true; break;
				case 0: sortir = true; break; 
				default: System.out.println(" - Opcio no valida -");
				}
			}else {
				Principal();
				int opc = sc.nextInt();
				System.out.println();
				switch(opc) {
				case 1: Ajuda.Ajuda();
				case 2: Jugador.def(jugador, Victories, Derrotes); break;
				case 3: Game.jugar(jugador,Victories, Derrotes); break;
				case 4: Jugador.dades(jugador, Victories, Derrotes); break;
				case 0: sortir = true; break;
				default: System.out.println(" - Opcio no valida -");
				}
			}
		}while(!sortir);
	}
	
	public static void Inici() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
	public static void Principal() {
		System.out.println("OPCIONS DEL PROGRAMA:");
		System.out.println("	1 - Mostra Ajuda");
		System.out.println("	2 - Definir Jugador");
		System.out.println("	3 - Jugar Partida");
		System.out.println("	4 - Dades Jugador");
		System.out.println("	0 - Sortir");
		System.out.println();
		System.out.print("Seleciona una opcio: ");
	}
	
}
