package Penjat;

public class Ajuda {
	
	public static void Ajuda() {
		System.out.println("\nBenvingut al joc del penjat\n");
		System.out.println("Per poder jugar, has de definir primer un jugador que posteriorment \npodràs veure les seves estadístiques a l'opció 4(Veure jugador).");
		System.out.println("Llistat de regles:\n- Màxim 5 errors.\n- Els noms dels jugadors han d'estar escrits en minúscules.\n- Has d'anar posant les lletres una per una.\n- Si poses una lletra dos cops, no entrarà, et demanarà una diferent.\n- Disfruta del joc");
		System.out.println();
	}
}
