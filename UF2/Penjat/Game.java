package Penjat;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
	
	static Scanner sc = new Scanner(System.in);

	public static void jugar(ArrayList<String> jugador, ArrayList<Integer> Victories, ArrayList<Integer> Derrotes) {
		String[] paraules = {"taula", "cadira", "pilota", "futbol", "gorra", "telefon", "ordinador", "camisa"};
		Random rm = new Random();
		String paraula = paraules[rm.nextInt(paraules.length)];
		ArrayList<Character> secreta = codificar(paraula);
		ArrayList<Character> lletres = new ArrayList<Character>();
		boolean acabat = false;
		final int maxErr = 5;
		int usErr = 0;
		
		while (!acabat) {
			Situacio(usErr, maxErr, secreta, lletres, jugador);
			char lletra = usades(lletres);
			lletres.add(lletra);
			usErr = comprovar(lletra, secreta, paraula, usErr);
			acabat = fi(lletra, secreta, paraula, usErr, maxErr, jugador, Victories, Derrotes);
		}
	}
	
	private static void Situacio(int usErr, int maxErr, ArrayList<Character> secreta, ArrayList<Character> lletres, ArrayList<String> jugador) {
		System.out.println("\nJugador actual: " + jugador.get(0));
		System.out.println("\nParaula actual: ");
		for (int i =0; i<secreta.size();i++) {
			System.out.print(secreta.get(i)+ " ");
		}
		System.out.print("\nErrors restants: ");
		System.out.print(maxErr-usErr + "\n");
		System.out.print("Lletres usades: ");
		if (secreta.size()==0)
			System.out.print("Encara no has escrit cap...\n");
		else
			for (int i = 0; i < lletres.size(); i++) {
				System.out.print(lletres.get(i)+" ");
			}
		
	}
	
	private static boolean fi(char lletra, ArrayList<Character>secreta, String paraula, int usErr, int maxErr, 
			ArrayList<String> jugador, ArrayList<Integer> Victories, ArrayList<Integer> Derrotes) {
		if (usErr >= maxErr) {
			System.out.println("T'has equivocat massa vegades, has perdut!!\nLa paraula era: " + paraula);
			System.out.println();
			Derrotes.set(0, Derrotes.get(0)+1);
			return true;
		}
		for (int i = 0; i<paraula.length();i++) {
			if (paraula.charAt(i)!=secreta.get(i))
				return false;
		}
		System.out.println("\nFELICITATS, HAS GUANYAT!!");
		Victories.set(0, Victories.get(0)+1);
		return true;
	}
	
	private static int comprovar(char lletraActual, ArrayList<Character> paraula_Usuari, String paraula_secreta, int errors_usuari) {
		boolean trobada=false;
		for (int i = 0; i < paraula_secreta.length(); i++) {
			if (paraula_secreta.charAt(i) == lletraActual) {
				paraula_Usuari.set(i, paraula_secreta.charAt(i));
				trobada = true;
			}
		}
		if (!trobada) {
			System.out.println("No es troba aquesta lletra.");
			return errors_usuari+1;
		}
		return errors_usuari;
	}
	
	public static char usades(ArrayList<Character> lletres) {
		boolean good = false;
		char lletra;
		do {
			System.out.print("\nEscriu una lletra: ");
			lletra = sc.nextLine().toLowerCase().charAt(0);
			if (lletres.contains(lletra)) 
				System.out.print("Ja has fet servir aquesta lletra, escull un altre.");
			else
				good=true;
		} while(!good);
		
		return lletra;
	}
	
	public static ArrayList<Character> codificar(String paraula) {
		ArrayList<Character> secreta = new ArrayList<Character>();
		for (int i = 0; i < paraula.length(); i++) {
			secreta.add('_');
		}
		return secreta;
	}
	
	
	
}
