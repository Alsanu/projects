package Penjat;

import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {

	public static void def(ArrayList<String> jugador, ArrayList<Integer> Victories, ArrayList<Integer> Derrotes) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Vols crear un nou jugador (Si creas un nou jugador s'eliminara l'anterior)? (SI) / (NO): ");
		String crear = sc.next().toUpperCase();
		if (crear.equals("SI")) {
			System.out.print("Quin nom tindra el jugador: ");
			jugador.add(sc.next());
			Victories.add(0);
			Derrotes.add(0);
		} else if (!crear.equals("NO")) {
			System.out.println("Opcio no valida");
		}
		System.out.println();
	}
	
	public static void dades(ArrayList<String> jugador, ArrayList<Integer> Victories, ArrayList<Integer> Derrotes) {
		System.out.println("*****************************");
		System.out.println("Nom:          " + jugador.get(0));
		System.out.println("Victories:    " + Victories.get(0));
		System.out.println("Derrotes:     " + Derrotes.get(0));
		System.out.println("*****************************");
		System.out.println();
	}
}
