package mokepons;

public class TipusDiferentException extends Exception {

	private static final long serialVersionUID = 1L;

	public TipusDiferentException(String message) {
        super(message);
    }
	
}
