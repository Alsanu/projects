package mokepons;

public abstract class Objecte {
	private String nom;
	protected int quantitat;
	
	public Objecte(String nom) {
		this.nom = nom;
		this.quantitat = 1;
	}
	
	public void obtenir(int numObj) {
		this.quantitat += numObj;
	}
	
	public void donar(MokeponCapturat mk) {
		mk.setObj(this);
	}
	
	abstract public void utilitzar(MokeponCapturat m);
	
	public String getNom() {
		return nom;
	}
	public int getQuantitat() {
		return quantitat;
	}
	
}
