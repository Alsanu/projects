package mokepons;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class Mokepon implements Comparable<Object>{

	Random rm = new Random();

	private String nom;
	private int nivell;
	private int atk;
	private int def;
	private int vel;
	private int exp; 
	private int max_exp;
	private int hp_max;
	private int hp_actual;
	private Tipus tipus;
	private ArrayList<Atacs> atacs = new ArrayList<Atacs>();
	private boolean debilitat;
	private Sexe sexe;

	public Mokepon() {
		this.nom = "Sense definir";
		this.nivell = 1;
		this.atk = 1;
		this.def = 1;
		this.vel = 1;
		this.max_exp = 100;
		this.hp_max = 10;
		this.hp_actual = 10;
		this.tipus = Tipus.WATER;
		this.debilitat = false;
		int sn = rm.nextInt(0,2);
		if (sn == 0) this.sexe = Sexe.MALE;
		else this.sexe = Sexe.FEMALE;
	}
	
	public Mokepon(String nom) {
		this();
		this.nom = nom;
	}
	
	public Mokepon(String nom, Sexe sexe) {
		this();
		this.nom = nom;
		this.sexe = sexe;
	}
	
	public Mokepon (String nom, Tipus tipus) {
		this();
		this.nom = nom;
		this.tipus = tipus;
	}
	
	public Mokepon(String nom, int nivell) {
		this();
		this.nom = nom;
		for (int i = 0; i < nivell; i++) {
			this.pujarNivell();
		}
	}
	
	public Mokepon(String nom, int nivell, int hp_max, int atk, int def, int vel, Tipus tipus){
		this();
		this.nom = nom;
		this.setNivell(nivell);
		this.hp_max = hp_max;
		this.hp_actual = this.hp_max;
		this.atk = atk;
		this.def = def;
		this.vel = vel;
		this.tipus = tipus;
	}
	
	private void pujarNivell() {
		this.nivell++;
		if (this.hp_actual == this.hp_max) {
			this.hp_max += rm.nextInt(0, 6);
			this.hp_actual = this.hp_max;
		}else {
			this.hp_max += rm.nextInt(0, 6);
			this.hp_actual += rm.nextInt(1, 4);
			if (this.hp_actual > this.hp_max) {
				this.hp_actual = this.hp_max;
			}
		}
		this.atk += rm.nextInt(0, 3);
		this.def += rm.nextInt(0, 3);
		this.vel += rm.nextInt(0, 3);
		this.max_exp += this.max_exp*0.25;
	}
	
	public void atorgarExperiencia(int exp_atorgada) {
		this.exp += exp_atorgada; 
		while (this.exp >= this.max_exp) {
			this.exp -= max_exp;
			pujarNivell();
		}
	}
	
	public void afegirAtac(Atacs at) {
		if (this.atacs.size() < 2) {
			this.atacs.add(at);
		}
	}
	
	public static double efectivitat(Tipus atac, Tipus defensa) {
        if(atac == Tipus.FIRE && defensa == Tipus.WATER ||atac == Tipus.WATER && defensa == Tipus.GRASS ||atac == Tipus.GRASS && defensa == Tipus.FIRE ) {
            return 0.5;
        }else if (atac == Tipus.WATER && defensa == Tipus.FIRE ||atac == Tipus.FIRE && defensa == Tipus.GRASS ||atac == Tipus.GRASS && defensa == Tipus.WATER ) {
            return 2;
        }else {
            return 1;
        }        
    }

	public void atacar(Mokepon atacat , int num_atac) {
		if (this.debilitat) System.out.println(this.nom + " esta debilitat i no pot atacar");
		else {
			Atacs at = this.atacs.get(num_atac-1);
			double bonus = efectivitat(at.tipus, atacat.tipus);
			double damage = ((((2 * this.nivell ) / 5 + 2 ) * at.poder * (this.atk / atacat.def)) / 50 + 2) * bonus;
			atacat.hp_actual -= (int) damage;
			debilitarse(atacat);
		}
	}
	
	public void debilitarse(Mokepon MK) {
		if (MK.hp_actual <= 0) MK.debilitat = true;
		else MK.debilitat = false;
	}
	
	public void curar(Mokepon MK) {
		MK.debilitat = false;
		MK.hp_actual = MK.hp_max;
	}
	
	public MokeponCapturat capturar(String nomEntrenador, String nomDonat) throws Exception {
		if(!(this instanceof MokeponCapturat)) {
			return (new MokeponCapturat(this, nomDonat, nomEntrenador));
		}
		else  {
			throw new MokeponJaCapturatException("No pots capturar un Mokepon que ja esta capturat");
		}
	}

	@Override
	public String toString() {
		String cadena = "";
		cadena += " - Name: " + this.nom + "\n";
		cadena += "Level: " + this.nivell + "\n";
		cadena += "Atack: " + this.atk + "\n";
		cadena += "Defense: " + this.def + "\n";
		cadena += "Speed: " + this.vel + "\n";
		cadena += "Exp: " + this.exp + "\n";
		cadena += "Exp. for next level : " + this.max_exp + "\n";
		cadena += "Hp: " + this.hp_actual + "\n";
		cadena += "Max.Hp: " + this.hp_max + "\n";
		cadena += "Type: " + this.tipus + "\n";
		cadena += "State: ";
		if (!this.debilitat) cadena += "Alive \n";
		else cadena += "Dead \n";
		if (atacs.size() > 0) {
		cadena += "================================ \n";
		cadena += "Attack list: \n";
			for (Atacs at: atacs) {
				cadena += at;
			}
		}
		return cadena;
	}

	@Override
	public int hashCode() {
		return Objects.hash(atacs, atk, debilitat, def, exp, hp_actual, hp_max, max_exp, nivell, nom, rm, sexe, tipus,
				vel);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mokepon other = (Mokepon) obj;
		if (nom.equals(other.nom) && atk == other.atk && debilitat == other.debilitat
				&& def == other.def && exp == other.exp && hp_actual == other.hp_actual && hp_max == other.hp_max
				&& max_exp == other.max_exp && nivell == other.nivell && sexe == other.sexe && tipus == other.tipus && vel == other.vel) {
			return true;
		}else return false;
	}
	
	@Override
	public int compareTo(Object arg0) {
		Mokepon altre = (Mokepon) arg0;
		if (altre.tipus == this.tipus) {
			if (altre.nom.equals(this.nom)) {
				return (altre.nivell - this.nivell);
			}
			else {
				return altre.nom.compareTo(this.nom);
			}
		}
		else {
			return(altre.getTipus().compareTo(this.getTipus()));
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNivell() {
		return nivell;
	}

	public void setNivell(int nivell) {
		for (int i = 1; i < nivell; i++) {
			this.pujarNivell();
		}
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getVel() {
		return vel;
	}

	public void setVel(int vel) {
		this.vel = vel;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getMax_exp() {
		return max_exp;
	}

	public void setMax_exp(int max_exp) {
		this.max_exp = max_exp;
	}

	public int getHp_max() {
		return hp_max;
	}

	public void setHp_max(int hp_max) {
		this.hp_max = hp_max;
	}

	public int getHp_actual() {
		return hp_actual;
	}

	public void setHp_actual(int hp_actual) {
		this.hp_actual = hp_actual;
		if (hp_actual < 0) this.hp_actual = 0;
		if (hp_actual > this.hp_max) this.hp_actual = this.hp_max;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public ArrayList<Atacs> getAtacs() {
		return atacs;
	}

	public void setAtacs(ArrayList<Atacs> atacs) {
		this.atacs = atacs;
	}

	public boolean isDebilitat() {
		return debilitat;
	}

	public void setDebilitat(boolean debilitat) {
		this.debilitat = debilitat;
	}
	
	public Sexe getSexe() {
		return sexe;
	}public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}
}
