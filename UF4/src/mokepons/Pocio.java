package mokepons;

public class Pocio extends Objecte{
	private int hp_curada;

	public Pocio(String nom) {
		super(nom);
	}
	
	Pocio(String nom, int quantitat){
		this(nom);
		this.quantitat = quantitat;
	}

	@Override
	public void utilitzar(MokeponCapturat m) {
		if (m.isDebilitat()) return;
		if (this.getQuantitat()<1) return;
		m.setHp_actual(m.getHp_actual() + hp_curada);
		this.quantitat--;
		
	}
	
	@Override  
	public String toString() {
		return (super.toString() + " Heal: " + this.hp_curada);
	}
	
	public int getHp_curada() {
		return hp_curada;
	}

	public void setHp_curada(int hp_curada) {
		this.hp_curada = hp_curada;
	}
}
