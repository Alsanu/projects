package mokepons;

public class MokeponJaCapturatException extends Exception {

	private static final long serialVersionUID = 1L;

	public MokeponJaCapturatException(String message) {
        super(message);
    }
	
}
