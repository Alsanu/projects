package hpCapitol3A;

public class GosTest {

	public static void main(String[] args) {
		Animal g1 = new Gos(3, "Gos1" , 0, AnimalSexe.FEMELLA, AnimalEstat.VIU);
		((Gos)g1).setRaza(new Raza("PastorAlemany", GosMida.GRAN, 12, true));
		g1.visualitzar();
		g1.so();
		
		Animal g2 = new Gos(5, "Gos2", 0, AnimalSexe.MASCLE, AnimalEstat.VIU);
		((Gos)g2).setRaza(new Raza("Yorsie", GosMida.PETIT, 16, false));
		g2.visualitzar();
		g2.so();
		
		Animal g3 = g1.aparellar(g2);
		if (g3 instanceof Gos) {
			g3.visualitzar();
			g3.so();
		}
		System.out.println("S'han creat " + Animal.getNumAnimals() + " animals al programa");
	}

}
