package hpCapitol3A;

public class Ganso extends Animal{
	
	private GansoTipus tipus;
	private static int edatTope = 6;
	
	public Ganso() {
		super();
		this.tipus = GansoTipus.DESCONEGUT;
	}
	
	public Ganso(String nom) {
		super(nom);
		this.tipus = GansoTipus.DESCONEGUT;
	}
	
	public Ganso(int edat, String nom, GansoTipus tipus, AnimalSexe sexe) {
		super(edat, nom, sexe);
		this.tipus = tipus;
	}
	
	public Ganso(Ganso g) {
		this();
		clonar(g);
	}
	
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "GANSO\n" + super.toString();
		if (tipus != null) cadena += "Tipus: " + getTipus() + "\n"
				+ "----------------------------";
		else cadena += "Tipus: desconegut" + "\n"
				+ "----------------------------";
		return cadena;
	}

	@Override
	public void visualitzar() {
		String cadena = "GANSO\n" + super.toString();
		if (tipus != null) cadena += "Tipus: " + getTipus() + "\n"
				+ "----------------------------";
		else cadena += "Tipus: desconegut" + "\n"
				+ "----------------------------";
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	

	public GansoTipus getTipus() {return tipus;}

	public void setTipus(GansoTipus tipus) {this.tipus = tipus;}
	public static int getEdatTope() {return edatTope;}
	
	@Override
	public void incEdat() {
		super.incEdat();
		if (this.tipus == GansoTipus.AGRESSIU && this.getEdat() >= (edatTope-1)) this.setEstat(AnimalEstat.MORT);
		if (this.tipus == GansoTipus.DOMESTIC && this.getEdat() >= (edatTope+1)) this.setEstat(AnimalEstat.MORT);
		if (this.tipus == GansoTipus.DESCONEGUT && this.getEdat() >= (edatTope)) this.setEstat(AnimalEstat.MORT);
	}
	
	@Override 
	public void so() {
		System.out.println("Quack, quack!");
	}
}
