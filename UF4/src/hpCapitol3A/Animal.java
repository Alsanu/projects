package hpCapitol3A;

import java.util.Random;

public class Animal {
	
	private static Random rm = new Random();
	private static int numAnimals;
	
	private int edat;
	private String nom;
	private int fills;
	private AnimalSexe sexe;
	private AnimalEstat estat;
	
	
	public Animal() {
		this.edat = 0;
		this.nom = "Sense Nom";
		this.fills = 0;
		this.sexe = rm.nextBoolean() ? AnimalSexe.MASCLE : AnimalSexe.FEMELLA;
		this.estat = AnimalEstat.VIU;
		numAnimals++;
	}
	
	public Animal(String nom) {
		this();
		this.nom = nom;
	}
	
	public Animal(int edat, String nom, AnimalSexe sexe) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.sexe = sexe;
	}
	
	public Animal(int edat, String nom, int fills, AnimalSexe sexe, AnimalEstat estat) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		this.estat = estat;
	}
	
	public Animal aparellar(Animal an) {
		if (this.estat == AnimalEstat.MORT || an.estat == AnimalEstat.MORT) return null;
		if (this.sexe == an.sexe) return null;
		this.fills++; an.fills++;
		Animal fill = new Animal();
		fill.setNom("Fill " + this.getNom() + " / " + an.getNom());
		return fill;
	}
	
	public void clonar(Animal an) {
		this.edat = an.edat;
		this.nom = an.nom;
		this.fills = an.fills;
		this.sexe = an.sexe;
		this.estat = an.estat;
	}
	
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena= "Edat: "+ edat + "\n" 
				+ "Nom: "+ nom + "\n" 
				+ "Fills: "+  fills + "\n" 
				+ "Sexe: "+ sexe + "\n"
				+ "Estat: " + estat + "\n"; 
		return cadena;
	}
	
	public void visualitzar() {
		String cadena= "Edat: "+ edat + "\n" 
				+ "Nom: "+ nom + "\n" 
				+ "Fills: "+ fills + "\n" 
				+ "Sexe: "+ sexe + "\n"
				+ "Estat: " + estat + "\n"; 
		cadena += "-----------------";	
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	/* +++++++++++++++++
	 * Getters & Setters
	 */
	public int getEdat() {return edat;}
	public void setEdat(int edat) {this.edat = edat;}

	public String getNom() {return nom;}
	public void setNom(String nom) {this.nom = nom;}

	public int getFills() {return fills;}
	public void setFills(int fills) {this.fills = fills;}

	public AnimalSexe getSexe() {return sexe;}
	public void setSexe(AnimalSexe sexe) {this.sexe = sexe;}

	public AnimalEstat getEstat() {return estat;}
	public void setEstat(AnimalEstat estat) {this.estat = estat;}

	public static int getNumAnimals() {return numAnimals;}
	/* Getters & Setters
	 * +++++++++++++++++
	 */
	
	public void incEdat() {
		if (this.estat == AnimalEstat.VIU) this.edat++;
	}
	
	public void so() {
		System.out.println("hola hola");
	}
}
