package hpCapitol3A;

public class Gos extends Animal{
	
	private Raza raza;
	
	public Gos() {
		super();
		this.raza = null;
	}
	
	public Gos(String nom) {
		super(nom);
		this.raza = null;
	}
	
	public Gos(int edat, String nom, int fills, AnimalSexe sexe, AnimalEstat estat) {
		super(edat, nom, fills, sexe, estat);
		this.raza = null;
	}
	
	public Gos(Gos g) {
		this();
		clonar(g);
	}
	
	@Override
	public Animal aparellar(Animal an) {
		if (!(an instanceof Gos)) return null;
		if (this.getEstat() != AnimalEstat.VIU || an.getEstat() != AnimalEstat.VIU) return null;;
		if (this.getSexe() == an.getSexe()) return null;
		
		Gos fill = new Gos("Fill " + this.getNom() + " / " + an.getNom()); 
		fill.setRaza(this.raza);
		return fill;
	}
	
	public void clonar(Gos g) {
		super.clonar(g);
		this.raza = g.raza;
	}
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "GOS\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		return cadena;
	}

	@Override
	public void visualitzar() {
		String cadena = "GOS\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	
	public Raza getRaza() {return raza;}
	public void setRaza(Raza raza) {this.raza = raza;}

	@Override
	public void incEdat() {
		super.incEdat();
		if (this.raza != null) {
			if (super.getEdat() > this.raza.getTempsVida()) super.setEstat(AnimalEstat.MORT);
		}else if (super.getEdat() >= 10) super.setEstat(AnimalEstat.MORT);
	}

	@Override
	public void so() {
		System.out.println("Guau, guau!");
	}
}
