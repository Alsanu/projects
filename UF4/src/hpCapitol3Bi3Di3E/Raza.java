package hpCapitol3Bi3Di3E;

public class Raza {
	
	private String nomRaza;
	private Mida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raza(String nom, Mida mida, int t) {
		this(nom, mida);
		this.tempsVida = t;
	}
	
	public Raza(String nom, Mida mida) {
		this.nomRaza = nom;
		this.mida = mida;
	}
	
	public Raza(String nom, Mida mida, int t, boolean dominant) {
		this(nom, mida, t);
		this.dominant = dominant;
	}
	
	@Override
	public String toString() {
		String cadena = "================================\n";
		cadena += " - Raza: " + this.nomRaza + "\n";
		cadena += " Mida: " + this.mida + "\n";
		cadena += " Temps de vida: " + this.tempsVida + "\n";
		cadena += " Dominant: " + this.dominant + "\n";
		cadena += "================================\n";
		return cadena;
	}

	/* Getters and Setters
	 * -------------------
	 */
	public String getNomRaza() {
		return nomRaza;
	}public void setNomRaça(String nomRaza) {
		this.nomRaza = nomRaza;
	}

	public Mida getMida() {
		return mida;
	}public void setMida(Mida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}

	public boolean isDominant() {
		return dominant;
	}public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}
	/* -------------------
	 * Getters and Setters
	 */
	
}
