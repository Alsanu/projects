package hpCapitol3Bi3Di3E;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	
	private static Random rm = new Random();
	
	private ArrayList<Animal> animals;
	private int numAnimals;
	private int topAnimals;

	public Granja() {
		animals = new ArrayList<Animal>();
		numAnimals = 0;
		topAnimals = 100;
	}
	
	public Granja(int top) {
		this();
		if (top > 100 || top < 1) this.topAnimals = 100;
		else this.topAnimals = top;
	}
	
	public int afegir(Animal a) {
		if (numAnimals < topAnimals) {
			this.animals.add(a);
			numAnimals++; return numAnimals;
		}else return -1;
	}
	
	public void generarGranja(int topAnimals) {
		int contGos = 1;
		int contGanso = 1;
		int contMico = 1;
		
		for(int i = 0 ; i < topAnimals ; i++) {
			
			switch (rm.nextInt(3)) {
			case 0: 
				afegir(new Gos(rm.nextInt(10), "Gos" + contGos, 0,(rm.nextBoolean()?Sexe.MASCLE:Sexe.FEMELLA)));
				contGos++; break;
			case 1: 
				switch (rm.nextInt(3)) {
				case 0:
					afegir(new Ganso(rm.nextInt(6), "Ganso" + contGanso, Tipus.AGRESSIU, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				case 1:
					afegir(new Ganso(rm.nextInt(6), "Ganso" + contGanso, Tipus.DOMESTIC, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				case 2:
					afegir(new Ganso(rm.nextInt(6), "Ganso" + contGanso, Tipus.DESCONEGUT, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				} contGanso++; break;
			case 2: 
				switch (rm.nextInt(3)) {
				case 0:
					afegir(new Mico(rm.nextInt(20), "Mico" + contMico, Tipus.AGRESSIU, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				case 1:
					afegir(new Mico(rm.nextInt(20), "Mico" + contMico, Tipus.DOMESTIC, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				case 2:
					afegir(new Mico(rm.nextInt(20), "Mico" + contMico, Tipus.DESCONEGUT, rm.nextBoolean()?Sexe.FEMELLA:Sexe.MASCLE)); break;
				} contMico++; break;
			default:
			}
		}
	}
	
	public Animal obtenirAnimal(int pos) {
		if(pos > animals.size())
			return null;
		else
			return animals.get(pos-1);
	}

	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "Maxim animals: " + topAnimals + "\nNumero de animals: " + numAnimals + "\n\n";
		for (Animal a: animals) {
			cadena += a.toString();
		}
		return (cadena);
	}
	
	public void visualitzar() {
		String cadena = "Maxim animals: " + topAnimals + "\nNumero de animals: " + numAnimals + "\n\n";
		for (Animal a: animals) {
			cadena += a.toString();
		}
		System.out.println(cadena);
	}

	public void visualitzarVius() {
		String cadena = "================================\n";
		for (Animal a: animals) {
			if (a.getEstat() == Estat.VIU) {
				cadena += a.toString();
			}
		}
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	public int getnumAnimals() { return numAnimals; }
	public int getTopAnimals() { return topAnimals; }
	
}
