package hpCapitol3Bi3Di3E;

public class Gos extends Animal implements Mamifer{
	
	private Raza raza;
	
	public Gos() {
		super();
		this.raza = null;
	}
	
	public Gos(String nom) {
		super(nom);
		this.raza = null;
	}
	
	public Gos(int edat, String nom, int fills, Sexe sexe) {
		super(edat, nom, fills, sexe);
		this.raza = null;
	}
	
	public Gos(Gos g) {
		this();
		clonar(g);
	}
	
	@Override
	public Animal aparellar(Animal an) {
		if (!(an instanceof Gos)) return null;
		if (this.getEstat() != Estat.VIU || an.getEstat() != Estat.VIU) return null;;
		if (this.getSexe() == an.getSexe()) return null;
		
		Gos fill = new Gos("Fill " + this.getNom() + " / " + an.getNom()); 
		fill.setRaza(this.raza);
		this.setFills(this.getFills() + 1);
		an.setFills(an.getFills() + 1);
		
		return fill;
	}
	
	public void clonar(Gos g) {
		super.clonar(g);
		this.raza = g.raza;
	}
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "\nGOS\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		return cadena;
	}

	@Override
	public void visualitzar() {
		String cadena = "\nGOS\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	
	public Raza getRaza() {return raza;}
	public void setRaza(Raza raza) {this.raza = raza;}

	@Override
	public void incEdat() {
		super.incEdat();
		if (this.raza != null) {
			if (super.getEdat() > this.raza.getTempsVida()) super.setEstat(Estat.MORT);
		}else if (super.getEdat() >= 10) super.setEstat(Estat.MORT);
	}

	@Override
	public void so() {
		System.out.println("Guau, guau!");
	}
	
	@Override
	public Animal saludar(Animal an) {
		Animal mort = null;
		if (an instanceof Ganso) {
			if (((Ganso) an).getTipus() == Tipus.AGRESSIU && this.getSexe() == Sexe.FEMELLA) {			
				this.setEstat(Estat.MORT);
				mort = this;
			}
			if (((Ganso) an).getTipus() == Tipus.DOMESTIC) {
				an.setEstat(Estat.MORT);
				mort = an;
			}
			if (((Ganso) an).getTipus() == Tipus.DESCONEGUT && this.getSexe() == Sexe.FEMELLA) {				
				an.setEstat(Estat.MORT);
				mort = an;
			}
		}else if (an instanceof Mico) {
			if (((Mico) an).getTipus() == Tipus.AGRESSIU) {			
				this.setEstat(Estat.MORT);
				mort = this;
			}
		}
		return mort;
	}

	@Override
	public void dormir() {
		System.out.println("El gos " + this.getNom() + " s'ha dormit");
	}

	@Override
	public void correr() {
		System.out.println("El gos " + this.getNom() + " ha començat a correr");
	}
	
}
