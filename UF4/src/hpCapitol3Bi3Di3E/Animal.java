package hpCapitol3Bi3Di3E;

import java.util.Random;

public abstract class Animal {
	
	private static Random rm = new Random();
	private static int numAnimals;
	
	private int edat;
	private String nom;
	private int fills;
	private Sexe sexe;
	private Estat estat;
	
	public Animal() {
		this.edat = 0;
		this.nom = "Sense Nom";
		this.fills = 0;
		this.sexe = rm.nextBoolean() ? Sexe.MASCLE : Sexe.FEMELLA;
		this.estat = Estat.VIU;
		numAnimals++;
	}
	
	public Animal(String nom) {
		this();
		this.nom = nom;
	}
	
	public Animal(int edat, String nom, Sexe sexe) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.sexe = sexe;
	}
	
	public Animal(int edat, String nom, int fills, Sexe sexe) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
	}
	
	public void clonar(Animal an) {
		this.edat = an.edat;
		this.nom = an.nom;
		this.fills = an.fills;
		this.sexe = an.sexe;
		this.estat = an.estat;
	}
	
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena= "Edat: "+ edat + "\n" 
				+ "Nom: "+ nom + "\n" 
				+ "Num. Fills: "+  fills + "\n" 
				+ "Sexe: "+ sexe + "\n"
				+ "Estat: " + estat + "\n"; 
		return cadena;
	}
	
	public void visualitzar() {
		String cadena= "Edat: "+ edat + "\n" 
				+ "Nom: "+ nom + "\n" 
				+ "Num. Fills: "+ fills + "\n" 
				+ "Sexe: "+ sexe + "\n"
				+ "Estat: " + estat + "\n"; 
		cadena += "-----------------";	
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	/* +++++++++++++++++
	 * Getters & Setters
	 */
	public int getEdat() {return edat;}
	public void setEdat(int edat) {this.edat = edat;}

	public String getNom() {return nom;}
	public void setNom(String nom) {this.nom = nom;}

	public int getFills() {return fills;}
	public void setFills(int fills) {this.fills = fills;}

	public Sexe getSexe() {return sexe;}
	public void setSexe(Sexe sexe) {this.sexe = sexe;}

	public Estat getEstat() {return estat;}
	public void setEstat(Estat estat) {this.estat = estat;}

	public static int getNumAnimals() {return numAnimals;}
	/* Getters & Setters
	 * +++++++++++++++++
	 */
	public void incEdat() {
		if (this.estat == Estat.VIU) this.edat++;
	}
	
	public abstract void so();
	public abstract Animal aparellar(Animal an);
	public abstract Animal saludar(Animal an);
}
