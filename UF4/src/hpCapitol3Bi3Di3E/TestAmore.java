package hpCapitol3Bi3Di3E;

public class TestAmore {

	public static void main(String[] args) {
		System.out.println("Inicialment tenim: " + Animal.getNumAnimals() + " animals.\n");
		
		Granja granja1 = new Granja(20);
		Granja granja2 = new Granja(20);
		Granja granjaNous = new Granja(100);
		Animal fill;
		
		granja1.generarGranja(20);
		
		System.out.println("\n\n********\nGranja 1\n********\n");
		granja1.visualitzar();
		granja2.generarGranja(20);
		System.out.println("\n\n********\nGranja 2\n********\n");
		granja2.visualitzar();
		
		for (int i = 1; i<=granja1.getnumAnimals();i++) {
			fill = granja1.obtenirAnimal(i).aparellar(granja2.obtenirAnimal(i));
			if (fill!=null)
				granjaNous.afegir(fill);
		}
		
		System.out.println("\n\n********\nBabyFarm 1\n********\n");
		granjaNous.visualitzar();

	}

}
