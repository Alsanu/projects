package hpCapitol3Bi3Di3E;

public class Ganso extends Animal implements Au{
	
	private Tipus tipus;
	private static int edatTope = 6;
	
	public Ganso() {
		super();
		this.tipus = Tipus.DESCONEGUT;
	}
	
	public Ganso(String nom) {
		super(nom);
		this.tipus = Tipus.DESCONEGUT;
	}
	
	public Ganso(int edat, String nom, Tipus tipus, Sexe sexe) {
		super(edat, nom, sexe);
		this.tipus = tipus;
	}
	
	public Ganso(Ganso g) {
		this();
		clonar(g);
	}
	
	@Override
	public Animal aparellar(Animal a) {
		if (!(a instanceof Ganso)) return null;
		if (this.getEstat() != Estat.VIU || a.getEstat() != Estat.VIU) return null;
		if (this.getSexe() == a.getSexe()) return null;
		
		Ganso fill = new Ganso("Fill de " + this.getNom() + " / " + a.getNom());
		fill.setTipus(this.getTipus());
		this.setFills(this.getFills() + 1);
		a.setFills(a.getFills() + 1);

		return fill;
	}
	
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "\nGANSO\n" + super.toString();
		if (tipus != null) cadena += "Tipus: " + getTipus() + "\n"
				+ "----------------------------";
		else cadena += "Tipus: desconegut" + "\n"
				+ "----------------------------";
		return cadena;
	}

	@Override
	public void visualitzar() {
		String cadena = "\nGANSO\n" + super.toString();
		if (tipus != null) cadena += "Tipus: " + getTipus() + "\n"
				+ "----------------------------";
		else cadena += "Tipus: desconegut" + "\n"
				+ "----------------------------";
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	

	public Tipus getTipus() {return tipus;}

	public void setTipus(Tipus tipus) {this.tipus = tipus;}
	public static int getEdatTope() {return edatTope;}
	
	@Override
	public void incEdat() {
		super.incEdat();
		if (this.tipus == Tipus.AGRESSIU && this.getEdat() >= (edatTope-1)) this.setEstat(Estat.MORT);
		if (this.tipus == Tipus.DOMESTIC && this.getEdat() >= (edatTope+1)) this.setEstat(Estat.MORT);
		if (this.tipus == Tipus.DESCONEGUT && this.getEdat() >= (edatTope)) this.setEstat(Estat.MORT);
	}
	
	@Override 
	public void so() {
		System.out.println("Quack, quack!");
	}
	
	@Override
	public Animal saludar(Animal an) {
		Animal mort = null;
		if (an instanceof Gos) {
			if (this.getTipus() == Tipus.AGRESSIU && an.getEdat() < 2) {			
				an.setEstat(Estat.MORT);
				mort =  an;
			}
			if (this.getTipus() != Tipus.AGRESSIU && an.getSexe() == Sexe.FEMELLA) {
				this.setEstat(Estat.MORT);
				mort = this;
			}			
		}else if (an instanceof Mico) {
			if (this.getTipus() != Tipus.AGRESSIU || ((Mico) an).getTipus() == Tipus.AGRESSIU) {
				this.setEstat(Estat.MORT);
				mort = this;
			}
		}
		return mort;
	}

	@Override
	public Ou pondre() {
		Ou ou = new Ou();
		System.out.println("L'au " + this.getNom() + " ha posat un ou");
		return ou;
	}

	@Override
	public void volar() {
		System.out.println("L'au " + this.getNom() + " ha començat a volar");
		
	}

}
