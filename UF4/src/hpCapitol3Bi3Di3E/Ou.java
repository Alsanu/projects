package hpCapitol3Bi3Di3E;

public class Ou {
	private static int numOus;
	private static int tempsEclosio = 100;
	private int tempsTranscorregut;
	
	public Ou() {
		this.tempsTranscorregut = 0;
		numOus++;
	}
	
	public Ou(int temps) {
		this();
		this.tempsTranscorregut = temps;
	}

	public static int getNumOus() {
		return numOus;
	}

	public int getTempsTranscorregut() {
		return tempsTranscorregut;
	}

	public void setTempsTranscorregut(int tempsTranscorregut) {
		this.tempsTranscorregut = tempsTranscorregut;
	}

	public static int getTempsEclosio() {
		return tempsEclosio;
	}
	
	
}
