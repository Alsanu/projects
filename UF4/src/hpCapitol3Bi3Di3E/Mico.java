package hpCapitol3Bi3Di3E;

public class Mico extends Animal implements Mamifer{
	
	private Raza raza;
	private Tipus tipus;
	
	public Mico() {
		super();
		this.raza = null;
	}
	
	public Mico(String nom) {
		super(nom);
		this.raza = null;
	}
	
	public Mico(int edat, String nom, int fills, Sexe sexe) {
		super(edat, nom, fills, sexe);
		this.raza = null;
	}
	
	public Mico(Mico m) {
		this();
		clonar(m);
	}
	
	public Mico(int edat, String nom, Tipus tipus, Sexe sexe) {
		super(edat, nom, sexe);
		this.tipus = tipus;
	}
	
	@Override
	public Animal aparellar(Animal an) {
		if (!(an instanceof Mico)) return null;
		if (this.getEstat() != Estat.VIU || an.getEstat() != Estat.VIU) return null;;
		if (this.getSexe() == an.getSexe()) return null;
		if (this.tipus == Tipus.AGRESSIU || ((Mico) an).tipus == Tipus.AGRESSIU ) return null; // MOD
		
		Mico fill = new Mico("Fill " + this.getNom() + " / " + an.getNom()); 
		fill.setRaza(this.raza);
		this.setFills(this.getFills() + 1);
		an.setFills(an.getFills() + 1);
		
		return fill;
	}
	
	public void clonar(Mico g) {
		super.clonar(g);
		this.raza = g.raza;
	}
	
	/* ++++++++++++++++++++++
	 * ToString & Visualitzar
	 */
	@Override
	public String toString() {
		String cadena = "\nMICO\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		return cadena;
	}

	@Override
	public void visualitzar() {
		String cadena = "\nMICO\n" + super.toString();
		if (raza != null) cadena += "Raza: " + raza.getNomRaza() + "\n------------------";
		else cadena += "Raza: desconeguda" + "\n------------------";
		System.out.println(cadena);
	}
	/* ToString & Visualitzar
	 * ++++++++++++++++++++++
	 */
	
	public Raza getRaza() {return raza;}
	public void setRaza(Raza raza) {this.raza = raza;}
	
	public Tipus getTipus() {return tipus;}
	public void setTipus(Tipus tipus) {this.tipus = tipus;}

	@Override
	public void incEdat() {
		super.incEdat();
		if (this.raza != null) {
			if (super.getEdat() > this.raza.getTempsVida()) super.setEstat(Estat.MORT);
		}else if (super.getEdat() >= 20) super.setEstat(Estat.MORT);
	}

	@Override
	public void so() {
		System.out.println("Ua ua ua!");
	}
	
	@Override
	public Animal saludar(Animal an) {
		Animal mort = null;
		if (this.tipus == Tipus.AGRESSIU) {
			if (an instanceof Mico) {
				if (((Mico) an).getTipus() == Tipus.AGRESSIU) {			
					if (this.getEdat() > an.getEdat()) {
						an.setEstat(Estat.MORT);
						mort = an;
					}else {
						this.setEstat(Estat.MORT);
						mort = this;
					}
				}
			}else {
				an.setEstat(Estat.MORT);
				mort = an;
			}
		}else {
			if (an instanceof Ganso) {
				if (((Ganso) an).getTipus() == Tipus.AGRESSIU && this.getSexe() == Sexe.FEMELLA) {			
					an.setEstat(Estat.MORT);
					mort = an;
				}
			}
		}
		return mort;
	}

	@Override
	public void dormir() {
		System.out.println("El mico " + this.getNom() + " s'ha dormit");
	}

	@Override
	public void correr() {
		System.out.println("El mico " + this.getNom() + " ha començat a correr");
	}
	
}
 