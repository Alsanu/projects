package hpCapitol3Bi3Di3E;

public class TestSaludar {
	
	public static void main(String[] args) {
		System.out.println("Inicialment tenim: " + Animal.getNumAnimals() + " animals.\n");
				
		Granja granja1 = new Granja(20);
		Granja granja2 = new Granja(20);
		Granja granjaNous = new Granja(20);
		Granja granjaMorts = new Granja(20);
		Animal resultat;
		
		granja1.generarGranja(20);
		
		System.out.println("\n\n********\nGranja 1\n********\n");
		granja1.visualitzar();
		granja2.generarGranja(20);
		System.out.println("\n\n********\nGranja 2\n********\n");
		granja2.visualitzar();
		
		for (int i = 1; i<=granja1.getnumAnimals();i++) {
			if (granja1.obtenirAnimal(i).getClass() == granja2.obtenirAnimal(i).getClass()){ 
				resultat = granja1.obtenirAnimal(i).aparellar(granja2.obtenirAnimal(i));
				if (resultat!=null)
					granjaNous.afegir(resultat);
			}
			else { 
				 resultat = granja1.obtenirAnimal(i).saludar(granja2.obtenirAnimal(i));
				if (resultat!=null)
					granjaMorts.afegir(resultat);
			}
		}
				
		System.out.println("\n********\nBabyFarm\n********\n");
		granjaNous.visualitzar();

		System.out.println("\n********\nDeathFarm\n********\n");
		granjaMorts.visualitzar();
		
		System.out.println();
		if (granjaNous.getnumAnimals() > granjaMorts.getnumAnimals()) System.out.println("L’amor ha triomfat");
		else if (granjaNous.getnumAnimals() < granjaMorts.getnumAnimals()) System.out.println("KAOS");
		else System.out.println("PEACE");
	}
}
