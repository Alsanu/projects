package hpCaptiol5;

import java.util.Random;

public class TestCapitolB {

	public static void main(String[] args) {
		Random rm = new Random();
		Gos Kenu = new Gos("Keni");
		Kenu.afegirTresor(new Llaminadura());
		Kenu.afegirTresor(new Os());
		
		Gos Juanjo = new Gos("Jom");
		Juanjo.afegirTresor(new Joguina());
		Juanjo.afegirTresor(new Os(true));
		
		Tresor temp1 = new Joguina();
		temp1 = Kenu.getTresorActiu();
		
		if (((Os)temp1).interCanviar(Juanjo.getTresorActiu())) {
			Kenu.getTresorActiu().clonar(Juanjo.getTresorActiu());
			Juanjo.getTresorActiu().clonar(temp1);
			System.out.println("S'ha realitzat el canvi");
		}else {
			System.out.println("No es pot intercanviar");
		}
		
		Gos Victor = new Gos("Visitor");
		
		for (int i = 0; i < 10; i++) {
			switch (rm.nextInt(3)) {
			case 0: Victor.afegirTresor(new Os()); break;
			case 1: Victor.afegirTresor(new Joguina()); break;
			case 2: Victor.afegirTresor(new Llaminadura()); break;
			default:
			}
		}
		
		System.out.println("Valors Comestibles Totals " + Victor.valorMenjar());
	}
}
