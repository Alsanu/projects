package hpCaptiol5;

public class Llaminadura extends Tresor implements Comestible {
	private String sabor;

	public Llaminadura() {
		this.sabor = "nuvol";
		this.setPrioritari(false);
		this.setValor(2);
		this.setDuracio(2);
	}
	
	public Llaminadura(String sabor) {
		this();
		this.sabor = sabor;
	}

	public Llaminadura(String sabor, boolean prioritari, int valor, int duracio) {
		this.sabor = sabor;
		this.setPrioritari(prioritari);
		this.setValor(valor);
		this.setDuracio(duracio);
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "Sabor: " + sabor;
		cadena += "\n===============================";
		return cadena;
	}
	
	public String getSabor() {return sabor;}
	public void setSabor(String sabor) {this.sabor = sabor;}
	
	@Override 
	public void enterrar() {
		super.enterrar();
		this.setValor(getValor()-1);
	}
	
	@Override 
	public void desenterrar() {
		super.desenterrar();
		this.setValor(getValor()-1);
	}
	
	public String assaborir() {
		return "Ummmm què bona aquesta llaminadura sabor " + this.sabor;
	}
	
	@Override
	public void clonar(Tresor t) {
		super.clonar(t);
		if (t instanceof Llaminadura) {
			this.sabor = ((Llaminadura) t).sabor;
		}
	}
}
