package hpCaptiol5;

public class Tresor implements Comparable<Object>{
	private boolean prioritari;
	private int valor;
	private int duracio;
	private boolean enterrat;
	private Tresor minim;
	
	public Tresor() {
		this.prioritari = false;
		this.enterrat = false;
		this.valor = 1;
		this.duracio = 1;
	}
	
	public Tresor(boolean prioritari, int valor, int duracio, boolean enterrat) {
		this.prioritari = prioritari;
		this.enterrat = enterrat;
		this.valor = valor;
		this.duracio = duracio;
	}
	
	public void clonar(Tresor t) {
		this.prioritari = t.prioritari;
		this.valor = t.valor;
		this.duracio = t.duracio;
		this.enterrat = t.enterrat;
	}

	@Override
	public String toString() {
		String cadena = "Prioritat: " + prioritari + "\n"
				+ "Valor: " + valor + "\n"
				+ "Duracio: " + duracio + "\n"
				+ "Enterrat: " + enterrat + "\n";
		return cadena;
	}
	
	/* +++++++++++++++++
	 * Getters & Setters
	 */
	public boolean isPrioritari() {return prioritari;}
	public void setPrioritari(boolean prioritari) {this.prioritari = prioritari;}
	
	public int getValor() {return valor;}
	public void setValor(int valor) {
		this.valor = valor;
		if (this.valor < 0) this.valor = 0;
	}
	
	public int getDuracio() {return duracio;}
	public void setDuracio(int duracio) {
		this.duracio = duracio;
		if (this.duracio < 0) this.duracio = 0;
	}
	
	public boolean isEnterrat() {return enterrat;}
	public void setEnterrat(boolean enterrat) {this.enterrat = enterrat;}
	/* Getters & Setters
	 * +++++++++++++++++
	 */
	
	public void enterrar() {
		this.enterrat = true;
	}
	
	public void desenterrar() {
		this.enterrat = false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null) return false;
		if (this.getClass() != obj.getClass()) return false;
		
		if (this.valor == ((Tresor)obj).valor && this.enterrat == ((Tresor)obj).enterrat) return true;
		
		int maxim = this.valor;
		minim = null;
		minim.clonar(this);
		if (this.valor > ((Tresor)obj).valor) minim.clonar(((Tresor)obj));
		else maxim = ((Tresor)obj).valor;
		if (maxim - minim.getValor() <= 2 && obj.getClass() == this.getClass() && minim.getValor() >=1) return true;
		
		if (this instanceof Comestible && obj instanceof Comestible && ((Llaminadura)minim).getSabor().equals("nuvol")) ;
		return false;
	}
	
	@Override
	public int compareTo(Object obj){
		Tresor a = (Tresor)obj;
		if(this.valor > a.valor) return 1;
		else if(this.valor < a.valor) return -1;
		else if(this.isPrioritari() && !a.isPrioritari()) return 1;
		else if(!this.isPrioritari() && a.isPrioritari()) return -1;
		else if(this instanceof Comestible && !(a instanceof Comestible)) return 1;
		else if(!(this instanceof Comestible) && a instanceof Comestible) return 1;
		else return 0;	
	}

}
