package hpCaptiol5;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	
	Random rm = new Random();
	
	private ArrayList<Gos> gossos;
	private int numGossos;
	private int topGossos;
	
	public void canviarPosicions(int posA, int posB) {
		Gos AUX = new Gos(this.obtenirGos(posA));
		this.obtenirGos(posA).clonar(this.obtenirGos(posB));
		this.obtenirGos(posB).clonar(AUX);
	}
	
	public void eliminar(int pos) {
		this.gossos.remove(pos-1);
		numGossos--;
	}

	public Granja() {
		gossos = new ArrayList<Gos>();
		numGossos = 0;
		topGossos = 100;
	}
	
	public Granja(int top) {
		this();
		if (top > 100 || top < 1) this.topGossos = 100;
		else this.topGossos = top;
	}
	
	public int afegir(Gos g) {
		if (numGossos < topGossos) {
			this.gossos.add(g);
			numGossos++; return numGossos;
		}else return -1;
	}
	
	public void generarGranja(int topGossos) {
		for(int i = 0 ; i < topGossos ; i++) {
			Gos g = new Gos(rm.nextInt(10), "Gos " + (i+1), 0,(rm.nextBoolean()?'M':'F'));
			int m = rm.nextInt(1, 4);
			GosMida gm;
			if (m == 1) gm = GosMida.PETIT;
			else if (m == 2) gm = GosMida.MITJA;
			else gm = GosMida.GRAN;
			Raza r = new Raza("Raza " + (i+1), gm, rm.nextInt(8, 21), rm.nextBoolean());
			g.setRaza(r);
			afegir(g);
		}
	}
	
	public Gos obtenirGos(int pos) {
		if(pos > gossos.size())
			return null;
		else
			return gossos.get(pos-1);
	}

	
	@Override
	public String toString() {
		String cadena = "\n\n";
		int cont = 1;
		for (Gos g: gossos) {
			cadena += " - Gos " + cont + "\n";
			cont++;
			cadena += g.toString();
		}
		return (cadena);
	}

	public void visualitzarVius() {
		String cadena = "================================\n";
		int cont = 1;
		for (Gos g: gossos) {
			if (g.getEstat() == GosEstat.VIU) {
				cadena += " - Gos " + cont + "\n";
				cadena += g.toString();
			}
			cont++;
		}
		System.out.println(cadena);
	}
	
	public int getNumGossos() { return numGossos; }
	public int getTopGossos() { return topGossos; }
	
}
