package hpCaptiol5;

import java.util.Random;

public class TestCapitolA {

	public static void main(String[] args) {
		Random rm = new Random();
		Gos Kenu = new Gos("Keni");
		int nTresors = rm.nextInt(1, 21);
		for (int i = 0; i < nTresors; i++) {
			switch (rm.nextInt(3)) {
			case 0: Kenu.afegirTresor(new Os()); break;
			case 1: Kenu.afegirTresor(new Joguina()); break;
			case 2: Kenu.afegirTresor(new Llaminadura()); break;
			default:
			}
		}
		
		for (int i = 0; i < rm.nextInt(5); i++) {
			Kenu.getTresorActiu().enterrar();
			Kenu.getTresorActiu().desenterrar();
			Kenu.renovar();
		}
		
		System.out.println(Kenu);
		System.out.println("\n+++++++++ LISTA POWER +++++++++");
		System.out.println("-------------- " + Kenu.powerTresors() + " -------------");
		
		System.out.println("\n+++++++++ TOTAL POWER +++++++++");
		System.out.println("-------------- " + Kenu.gosPower() + " -------------");
	}

}
