package hpCaptiol5;

public class Os extends Tresor implements Comestible, Intercanviable{
	private boolean superOs;

	public boolean isSuperOs() {
		return superOs;
	}

	public Os() {
		this.superOs = false;
		this.setPrioritari(false);
		this.setValor(4);
		this.setDuracio(4);
	}
	
	public Os(boolean superOs) {
		this.superOs = superOs;
		if (this.superOs == true) {
			this.setPrioritari(false);
			this.setValor(4);
			this.setDuracio(4);
		}
	}
	
	public Os(boolean superOs, boolean prioritari, int valor, int duracio) {
		this.superOs = superOs;
		this.setPrioritari(prioritari);
		this.setValor(valor);
		this.setDuracio(duracio);
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "Super Os: " + superOs;
		cadena += "\n===============================";
		return cadena;
	}

	public void setSuperOs(boolean superOs) {this.superOs = superOs;}
	
	@Override
	public void enterrar() {
		super.enterrar();
		if (!this.superOs) {
			this.setValor(getValor()-1);
			this.setDuracio(getDuracio()-1);
		}
	}
	
	@Override
	public void desenterrar() {
		super.desenterrar();
		this.setValor(getValor()-1);
		this.setDuracio(getDuracio()-1);
	}

	@Override
	public boolean interCanviar(Tresor t) {
		if (t instanceof Os && ((Os) t).isSuperOs()) return true;
		return false;
	}
	
	@Override
	public void clonar(Tresor t) {
		super.clonar(t);
		if (t instanceof Os) {
			this.superOs = ((Os) t).superOs;
		}
	}
}
