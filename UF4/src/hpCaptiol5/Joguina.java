package hpCaptiol5;

public class Joguina extends Tresor implements Intercanviable {
	private JEstat estat;
	
	public Joguina() {
		this.estat = JEstat.NORMAL;
		this.setPrioritari(false);
		this.setValor(2);
		this.setDuracio(1000);
	}
	
	public Joguina(JEstat estat) {
		this.estat = estat;
		if (this.estat == JEstat.NOU) {
			this.setPrioritari(true);
			this.setValor(6);
			this.setDuracio(1000);
		}else {
			this.setPrioritari(false);
			this.setValor(6);
			this.setDuracio(500);
		}
	}

	public Joguina(JEstat estat, boolean prioritari, int valor, int duracio) {
		this.estat = estat;
		this.setPrioritari(prioritari);
		this.setValor(valor);
		this.setDuracio(duracio);
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "Estat: " + estat;
		cadena += "\n===============================";
		return cadena;
	}
	
	public JEstat getEstat() {return estat;}
	public void setEstat(JEstat estat) {this.estat = estat;}
	
	@Override
	public void enterrar() {
		super.enterrar();
		if (this.estat != JEstat.NOU) {this.setValor(getValor()-1);}
	}
	
	@Override
	public void desenterrar() {
		super.desenterrar();
		this.setValor(getValor()-1);
	}

	@Override
	public boolean interCanviar(Tresor t) {
		if (t instanceof Intercanviable && t.getValor() >= (this.getValor()*0.75)) return true;
		return false;
	}
	
	@Override
	public void clonar(Tresor t) {
		super.clonar(t);
		if (t instanceof Joguina) {
			this.estat = ((Joguina) t).estat;
		}
	}
}
