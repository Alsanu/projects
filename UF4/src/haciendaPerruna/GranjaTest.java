package haciendaPerruna;

public class GranjaTest {

	public static void main(String[] args) {
		Granja cinto;
		
		cinto = new Granja(20);
		System.out.println("Inicialment, la granja buida: ");
		System.out.println(cinto);
		
		cinto.generarGranja(5);
		System.out.println("Després d'afegir 5 gossos: ");
		System.out.println(cinto);
		
		Raza r = new Raza("Husky", GosMida.GRAN, 10);
		Gos kk = new Gos(13,"Starky",0,'M');
		kk.setRaza(r);
		cinto.afegir(kk);
		System.out.println("Després d'afegir a Starky: ");
		System.out.println(cinto);

		System.out.println("Gossos VIUS: ");
		cinto.visualitzarVius();
		cinto.obtenirGos(8);
		
	}

}
