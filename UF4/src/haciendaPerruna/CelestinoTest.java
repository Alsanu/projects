package haciendaPerruna;

import java.util.Random;
import java.util.Scanner;

public class CelestinoTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random rm = new Random();
		
		Granja granja = new Granja();
		granja.generarGranja(10);
		Granja cementeri = new Granja();
		
		for (int i = 0; i < 10; i++) {
			int errors = 0;
			while (errors < 5) {
				Gos g1 = new Gos(); g1.clonar(granja.obtenirGos(rm.nextInt(1, granja.getNumGossos()+1)));
				Gos g2 = new Gos(); boolean fi = false; 
				while(!fi) {
					g2.clonar(granja.obtenirGos(rm.nextInt(1, granja.getNumGossos()+1)));
					if (g1 != g2) fi = true;
				}
				Gos g3; g3 = g2.aparellar(g1);
				if (g3 == null) errors++;
				else granja.afegir(g3);
			}
			for (int x = 1; x < granja.getNumGossos(); x++) {
				granja.obtenirGos(x).setEdat(granja.obtenirGos(x).getEdat() + 1);
				if (granja.obtenirGos(x).getEstat() == GosEstat.MORT) {
					cementeri.afegir(granja.obtenirGos(x));
					granja.eliminar(x);
				}
			}
			System.out.println(granja);
			System.out.println(cementeri);
			
			System.out.println("\n\n++++++++++++++++++ FILLS I PARES ++++++++++++++++++\n\n");
			for (int x = 1; x < granja.getNumGossos(); x++) {
				System.out.println(" - Nom: " + granja.obtenirGos(x).getNom());
				Gos[] pares = granja.obtenirGos(x).pares();
				System.out.println("Pare: " + pares[0].getNom());
				System.out.println("Mare: " + pares[1].getNom());
			}
			System.out.println("\n\n++++++++++++++++++ FILLS I PARES ++++++++++++++++++\n\n");
		}

	}

}
