package haciendaPerruna;

import java.util.ArrayList;
import java.util.Random;

public class Gos {
	
	Random rm = new Random();
	public static int qGossos;
	
	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private Raza raza;
	private GosEstat estat;
	
	private Gos pare;
	private Gos mare;

	public Gos() {
		qGossos++;
		this.setEdat(rm.nextInt(1,11));
		this.nom = "gos";
		this.fills = 0;
		boolean s = rm.nextBoolean();
		if (s) this.sexe = 'M';
		else this.sexe = 'F';
		this.estat = GosEstat.VIU;
		this.pare = new Gos(10, "Desconegut", 1, 'M', GosEstat.MORT);
		this.mare = new Gos(10, "Desconegut", 1, 'F', GosEstat.MORT);
	}
	
	public Gos(String nom) {
		this();
		this.nom = nom;
	}
	
	public Gos(String nom, int edat) {
		this();
		this.nom = nom;
		this.setEdat(edat);
	}
	
	public Gos(int edat, String nom, int fills, char sexe) {
		this();
		this.nom = nom;
		this.setEdat(edat);
		this.fills = fills;
		this.sexe = sexe;
	}
	
	public Gos(int edat, String nom, int fills, char sexe, GosEstat estat) {
		this.nom = nom;
		this.setEdat(edat);
		this.fills = fills;
		this.sexe = sexe;
		this.estat = estat;
	}
	
	/* Clonacion - Copia
	 * -----------------
	 */
	public Gos(Gos g) {
		this.nom = g.nom;
		this.setEdat(g.edat);
		this.fills = g.fills;
		this.sexe = g.sexe;
		this.raza = g.raza;
		this.estat = g.estat;
	}
	
	public void clonar(Gos g) {		
		this.setEdat(g.edat);
		this.nom= g.nom;
		this.fills= g.fills;
		this.sexe= g.sexe;
	}
	/* -----------------
	 * Clonacion - Copia
	 */
	
	public Gos(Raza raza) {
		this();
		this.raza = raza;
	}
	
	public Gos[] pares() {
		Gos[] pares = {this.pare, this.mare};
		return pares;
	}
	
	public ArrayList<Gos> fills(Granja g) {
		ArrayList<Gos> fills = new ArrayList<Gos>();
		for (int i = 1; i < g.getNumGossos(); i++) {
			if (g.obtenirGos(i).pare == this || g.obtenirGos(i).mare == this) {
				fills.add(g.obtenirGos(i));
			}
		}
		return fills;
	}
	
	public boolean germa(Gos g) {
		if (this.pare == g.pare || this.mare == g.mare) return true;
		return false;
	}

	public static void quantitatGosos() {
		System.out.println("Hi ha " + qGossos + " de gossos");
	}
	
	public Gos aparellar(Gos g) {
		if (g == null) return null;
		if (this.germa(g)) return null;
		if ((this.edat < 2 || this.edat > 10) || (g.edat < 2 || g.edat > 10)) return null;
		if (this.sexe == g.sexe) return null;
		if (this.estat == GosEstat.MORT || g.estat == GosEstat.MORT) return null;
		if (this.sexe == 'F' && this.fills > 3 || g.sexe == 'F' && g.fills > 3) return null;
		Gos fill = new Gos();
		Raza r = null;
		if (this.raza == null && g.raza != null) r = g.raza;
		else if (this.raza != null && g.raza == null) r = this.raza ;
		else if (this.raza != null && g.raza != null) {
			
			int primer = mida(this); int segon = mida(g);
			
			if (this.sexe == 'F' && segon > primer || g.sexe == 'F' && segon < primer) return null;

			if (this.sexe == 'F' && this.raza.isDominant()) r = this.raza;
			else if (g.sexe == 'F' && this.raza.isDominant()) r = g.raza;
			else if (g.sexe == 'F' && !this.raza.isDominant() && this.raza.isDominant()) r = this.raza;
			else if (this.sexe == 'F' && !this.raza.isDominant() && g.raza.isDominant()) r = g.raza;
			else if (this.sexe == 'F') r = this.raza;
			else r = g.raza;
		}
	
		String nomPare = "";
		String nomMare = "";
		if (this.sexe == 'F') {
			nomPare = g.nom;
			nomMare = this.nom;
		}else {
			nomPare = this.nom;
			nomMare = g.nom;
		}
		
		if (rm.nextBoolean()) {
			fill.sexe = 'M';
			fill.nom = "Fill de " + nomPare;
			fill.pare = this;
			fill.mare = g;
		}else {
			fill.sexe = 'F';
			fill.nom = "Fill de " + nomMare;
			fill.pare = g;
			fill.mare = this;
		}
		fill.edat = 0;
		fill.raza = r;
		this.fills++;
		g.fills++;
		return fill;
	}
	
	public int mida(Gos g) {
		switch (g.raza.getMida()) {
		case PETIT: return 1; 
		case MITJA: return 2; 
		case GRAN: return 3;
		default: return 0;
		}
	}
	
	@Override
	public String toString() {
		String cadena = "================================\n";
		cadena += " - Nom: " + this.nom + "\n";
		cadena += " Edat: " + this.edat + "\n";
		cadena += " Fills: " + this.fills + "\n";
		cadena += " Sexe: " + this.sexe + "\n";
		cadena += " Estat: " + this.estat + "\n";
		if (raza!=null) { cadena += raza.toString(); }
		else { cadena += "Raza desconeguda\n"; 
			cadena += "================================\n"; }
		return cadena;
	}
	
	public void visualitzar() {
		borda();
		System.out.println("================================\n"
							+ "nom: "+ nom + "\n" 
							+ "edat: "+ edat + "\n" 
							+ "fills: "+ fills + "\n" 
							+ "sexe: "+ sexe + " \n"
							+ "================================\n");
	}


	/* Getters and Setters
	 * -------------------
	 */
	public int getEdat() { return edat; }
	public void setEdat(int edat) { 
		this.edat = edat;
		if (this.raza != null) {
			if (this.edat > this.raza.getTempsVida()) this.estat = GosEstat.MORT;
		}else if (this.edat > 10) this.estat = GosEstat.MORT;
	}
	
	public String getNom() { return nom; }
	public void setNom(String nom) { this.nom = nom; }
	
	public int getFills() { return fills; }
	public void setFills(int fills) { this.fills = fills; }
	
	public char getSexe() { return sexe; }
	public void setSexe(char sexe) { this.sexe = sexe; }
	
	public Raza getRaza() { return raza; } 
	public void setRaza(Raza raza) { this.raza = raza; }

	public GosEstat getEstat() { return estat; }
	public void setEstat(GosEstat estat) { this.estat = estat; }
	
	public Gos getPare() { return pare; }
	public void setPare(Gos pare) { this.pare = pare;}

	public Gos getMare() { return mare; }
	public void setMare(Gos mare) { this.mare = mare; }
	/* -------------------
	 * Getters and Setters
	 */
	 
	public void borda() {
		System.out.println("guau guau");
	}
	
}
