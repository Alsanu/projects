package haciendaPerruna;

public class AparellarTest {

	public static void main(String[] args) {
		Granja g1 = new Granja();
		Granja g2 = new Granja();
		Granja g3 = new Granja();
		Gos f; 
		
		g1.generarGranja(100);
		g2.generarGranja(100);
		
		for (int i=1;i<=g1.getNumGossos();i++) {
			f = g1.obtenirGos(i).aparellar(g2.obtenirGos(i));
			if (f != null) g3.afegir(f);
		}
		
		System.out.println(g3);
	}

}
