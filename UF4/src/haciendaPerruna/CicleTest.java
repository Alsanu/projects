package haciendaPerruna;

import java.util.Scanner;

public class CicleTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Cuants anys vols simular a la Granja: ");
		int anys = sc.nextInt();
		
		Granja granja = new Granja();
		granja.generarGranja(10);
		System.out.println(granja);
		Gos f;
		
		for (int i = 0; i < anys; i++) {
			for (int x = 1; x < granja.getNumGossos(); x++) {
				f = granja.obtenirGos(x).aparellar(granja.obtenirGos(x+1));
				if (f != null) {
					granja.afegir(f);
					System.out.println("\n******** Ha nascut: \n" + granja.obtenirGos(granja.getNumGossos()) + "\n");
					granja.obtenirGos(x).setEdat(granja.obtenirGos(x).getEdat() + 1);
					granja.obtenirGos(x+1).setEdat(granja.obtenirGos(x+1).getEdat() + 1);
					granja.obtenirGos(x).setFills(granja.obtenirGos(x).getFills() + 1);
					granja.obtenirGos(x+1).setFills(granja.obtenirGos(x+1).getFills() + 1);
					x++;
				}else {
					granja.canviarPosicions(x, x+1);
					granja.obtenirGos(x).setEdat(granja.obtenirGos(x).getEdat() + 1);
				}
			}
			System.out.println(granja);
		}

	}

}
