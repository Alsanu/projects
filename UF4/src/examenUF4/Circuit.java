package examenUF4;

import java.util.Random;

public class Circuit {
	static Random rm = new Random();
	
	public Objecte[] caselles;
	
	public Circuit(int nObjectes) {
		this.caselles = new Objecte[50];
		for (int i = 0; i < 50; i++) {
			caselles[i] = null;
		}
		if (nObjectes > 20) nObjectes = 20;
		for (int i = 0; i < nObjectes; i++) {
			int c = rm.nextInt(50);
			if (caselles[c] == null) {
				switch (rm.nextInt(3)) {
				case 0: caselles[c] = new Moneda(c); break;
				case 1: caselles[c] = new Platan(); break;
				case 2: caselles[c] = new Neumatic(); break;
				default:
				}
			}
		}
	}
	
	public void visualitar() {
		String cadena = "==============\n";
		cadena += "Circuit: \n\n";
		for (int i = 0; i < 50; i++) {
			if (caselles[i] != null) {
				cadena += caselles[i];
				cadena += "==============\n";
			}
		}
		System.out.println(cadena);
	}
}
