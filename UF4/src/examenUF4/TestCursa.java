package examenUF4;

public class TestCursa {

	public static void main(String[] args) {
		Circuit IES = new Circuit(20);
		
		IES.visualitar();
		
		MarioTeam Kenu = new MarioTeam("Kenu");
		KoopalingTeam Marcel = new KoopalingTeam("Marcel");
		
		System.out.println(Kenu + "\n\n" + Marcel);
		for (int i = 0; i < 50; i++) {
			if (!Kenu.avansar(IES, 1)) {
				System.out.println("Kenu ha terminat la cursa\n");
			}else {
				if (!Marcel.avansar(IES, 1)) {
					System.out.println("Marcel ha terminat la cursa\n");
				}
			}
		}
		
		
		System.out.println(Kenu + "\n" + Marcel);
		
		KoopalingTeam Juanjo = new KoopalingTeam("Juanjo");
		Juanjo.posicio = 5;
		Marcel.posicio = 1;
		Marcel.elMeuObjecte = new Platan();
		
		if (!Juanjo.avansar(IES, 6)) {
			System.out.println("Juanjo ha terminat la cursa");
		}else {
			if (!Marcel.avansar(IES, 3)) {
				System.out.println("Marcel ha terminat la cursa");
			}else {
				Marcel.chocar(Juanjo);
				if (Kenu.posicio == 0) {
					System.out.println("Marcel ha chocat exitosament a Juanjo");
				}else {
					System.out.println("Marcel no ha conseguit chocar a Juanjo");
				}
			}
		}
		
		Objecte moneda = new Moneda(20);
		Objecte neumatic = new Neumatic();
		int num = moneda.compareTo(neumatic);
		
		if (num == 1) {
			System.out.println("\n" + moneda + " \n Es mes gran que: \n\n" + neumatic);
		}else if (num == 0) {
			System.out.println("\n" + moneda + " \n Es igual que: \n\n" + neumatic);
		}else {
			System.out.println("\n" + moneda + " \n Es mes petit que: \n\n" + neumatic);
		}
		
	}

}
