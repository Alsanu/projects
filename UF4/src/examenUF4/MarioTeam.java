package examenUF4;

public class MarioTeam extends Pilot{
	
	public MarioTeam(String nom) {
		super(nom);
	}

	@Override
	public void agafarObjecte(Objecte obj) {
		if (this.elMeuObjecte == null) {
			this.elMeuObjecte = obj;
		}else {
			if (!this.elMeuObjecte.isPrioritari() && obj.isPrioritari()) this.elMeuObjecte = obj;
			if (this.elMeuObjecte instanceof Moneda && obj instanceof Moneda) ((Moneda)elMeuObjecte).valor += ((Moneda)obj).valor;
			if (this.elMeuObjecte instanceof Platan && obj instanceof Platan) this.elMeuObjecte = obj;
		}
	}

	@Override
	public String cridar() {
		return "IUUUUUJUUUUUU";
	}
	
	@Override
	public String toString() {
		String cadena = "Team Mario\n";
		cadena += super.toString();
		return cadena;
	}
	
}
