package examenUF4;

import java.util.Random;

public abstract class Objecte implements Comparable<Object> {
	static Random rm = new Random();
	
	protected boolean prioritari;
	protected TipusObjecte tObj;
	
	public Objecte() {
		this.prioritari = false;
	}
	
	/* +++++++++++++++++
	 * GETTERS & SETTERS
	 */
	
	public boolean isPrioritari() {return prioritari;}
	public void setPrioritari(boolean prioritari) {this.prioritari = prioritari;}

	public TipusObjecte gettObj() {return tObj;}
	public void settObj(TipusObjecte tObj) {this.tObj = tObj;}
	
	/* GETTERS & SETTERS
	 * +++++++++++++++++
	 */
	
	public void llençar() {
		System.out.println("M'han llençat");
	}
	
	@Override
	public int compareTo(Object obj){
		Objecte a = (Objecte) obj;
		if(this instanceof Moneda && !(a instanceof Platan)) return 1;
		if (this instanceof Moneda && !(a instanceof Neumatic)) return 1;
		if(this instanceof Platan && !(a instanceof Moneda)) return -1;
		if (this instanceof Platan && !(a instanceof Neumatic)) return 1;
		if(this instanceof Neumatic && !(a instanceof Moneda)) return -1;
		if (this instanceof Neumatic && !(a instanceof Platan)) return -1;
		return 0;
	}
}
