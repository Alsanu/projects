package examenUF4;

public abstract class Pilot {
	public String nom;
	public int posicio;
	public Objecte elMeuObjecte;
	
	public Pilot(String nom) {
		this.posicio = 0;
		this.elMeuObjecte = null;
	}
	
	public boolean avansar(Circuit c, int incrementar) {
		this.posicio += incrementar;
		if (this.posicio >= 50) {
			cridar();
			return false;
		}
		if (c.caselles[this.posicio] != null) {
			this.agafarObjecte(c.caselles[this.posicio]);
			if (c.caselles[this.posicio] instanceof Moneda) c.caselles[this.posicio] = null;
		}
		return true;
	}
	
	@Override
	public String toString() {
		String cadena = "Pilot \n"
				+ " - Nom: " + this.nom + "\n"
				+ " - Posicio: " + this.posicio + "\n"
				+ " - Objecte: " + this.elMeuObjecte + "\n";
		return cadena;
	}
	
	public abstract void agafarObjecte(Objecte obj);
	public abstract String cridar();

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		return true;
	}
	
}
