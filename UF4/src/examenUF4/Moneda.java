package examenUF4;

public class Moneda extends Objecte {
	public int valor;
	
	public Moneda(int valor) {
		super();
		this.valor = valor;
		super.prioritari = true;
		super.tObj = TipusObjecte.PREMI;
	}

	@Override
	public String toString() {
		String cadena = "Moneda \n"
				+ " - Valor: " + this.valor + "\n";
		return cadena;
	}
	
}
