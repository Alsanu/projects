package examenUF4;

public class Neumatic extends Objecte{
	public boolean superNeumatic;
	
	public Neumatic() {
		super();
		this.superNeumatic = rm.nextBoolean();
		super.prioritari = false;
		super.tObj = TipusObjecte.DEFENSA;
	}
	
	@Override
	public String toString() {
		String cadena = "Neumatic \n"
				+ " - Super Neumatic: " + this.superNeumatic + "\n";
		return cadena;
	}
}
