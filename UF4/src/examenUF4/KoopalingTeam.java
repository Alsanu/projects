package examenUF4;

public class KoopalingTeam extends Pilot implements Malote {

	public KoopalingTeam(String nom) {
		super(nom);
	}
	
	@Override
	public String toString() {
		String cadena = "Team Mario\n";
		cadena += super.toString();
		return cadena;
	}

	@Override
	public void agafarObjecte(Objecte obj) {
		if (this.elMeuObjecte == null) {
			this.elMeuObjecte = obj;
		}else {
			if (this.elMeuObjecte instanceof Neumatic && obj instanceof Neumatic) {
				if (((Neumatic)obj).superNeumatic) this.elMeuObjecte = obj;
			}
			if (this.elMeuObjecte instanceof Neumatic && !(obj instanceof Neumatic)) {
				this.elMeuObjecte = obj;
			}
		}
	}

	@Override
	public String cridar() {
		return "SIIIIIII";
	}

	@Override
	public void chocar(Pilot P) {
		if (this.equals(P)) {	// +++++++++++++++++++++++++++++++++ CANVIAT +++++++++++++++++++++++++++++++++++++++
			if (this.elMeuObjecte == null) this.posicio = 0;
			else if (this.elMeuObjecte != null && P.elMeuObjecte == null) P.posicio = 0;
			else {
				if (this.elMeuObjecte instanceof Platan) {
					P.posicio = 0;
					this.elMeuObjecte = null;
				}
				else if (this.elMeuObjecte instanceof Neumatic) this.elMeuObjecte = null;
				else if (this.elMeuObjecte instanceof Moneda && P.elMeuObjecte instanceof Moneda) {
					Objecte pM = P.elMeuObjecte;
					((Moneda)elMeuObjecte).valor += ((Moneda)pM).valor;
					((Moneda)pM).valor = 0; P.elMeuObjecte = pM;
				}
			}
		}
	}
}
