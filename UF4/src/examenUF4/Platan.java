package examenUF4;

public class Platan extends Objecte{
	static int totalPlatans = 0;
	public int numPlatan;
	
	public Platan() {
		super();
		totalPlatans++;
		this.numPlatan = totalPlatans;
		super.prioritari = false;
		super.tObj = TipusObjecte.ATAC;
	}
	
	@Override 
	public void llençar() {
		System.out.println("Toma platanaco");
	}
	
	@Override
	public String toString() {
		String cadena = "Platan \n"
				+ " - Platans totals : " + totalPlatans + "\n"
				+ " - Platan numero: " + this.numPlatan + "\n";
		return cadena;
	}
}
