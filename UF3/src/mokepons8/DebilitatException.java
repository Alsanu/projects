package mokepons8;

public class DebilitatException extends Exception {

	private static final long serialVersionUID = 1L;

	public DebilitatException(String message) {
        super(message);
    }
	
}
