package mokepons8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class FitxersSerialitzats {
	
	public static void afegirObjecte(Objecte obj) {
		try {
			File f = new File("objectes.dat");
			FileOutputStream fos = new FileOutputStream(f, true);
			AppendableObjectOutputStream aoos = new AppendableObjectOutputStream(fos, true);
			aoos.writeObject(obj);
			aoos.flush();
			aoos.close();
			fos.close();
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public static Object recuperarObjecte() {
		try {
			File f = new File("objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();
			ois.close();
			
			if (obj instanceof Pocio) return ((Pocio) obj);
			else if (obj instanceof Reviure) return ((Reviure) obj);
			else if (obj instanceof Arma) return ((Arma) obj);
			else if (obj instanceof Armadura) return ((Armadura) obj);
			
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
		return null;
	}

	public static List<Object> recuperarObjectes() {
		List<Object> llista = new ArrayList<Object>();
		try {
			File f = new File("objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object obj = ois.readObject();
				if (obj instanceof Pocio) llista.add((Pocio) obj);
				else if (obj instanceof Reviure) llista.add((Reviure) obj);
				else if (obj instanceof Arma) llista.add((Arma) obj);
				else if (obj instanceof Armadura) llista.add((Armadura) obj);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
		return llista;
	}

	public static Pocio recuperaPocioConcreta(int n) {
		try {
			File f = new File("objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object obj = ois.readObject();
				if (obj instanceof Pocio)
					if (((Pocio) obj).getHp_curada() == n) return (Pocio) obj;
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
		return null;
	}

	
	public static void afegeixMokepon(MokeponCapturat mok) {
		try {
			if (!isMokepon(mok)) {
				File f = new File("objectes.dat");
				FileOutputStream fos = new FileOutputStream(f, true);
				AppendableObjectOutputStream aoos = new AppendableObjectOutputStream(fos, true);
				aoos.writeObject(mok);
				aoos.close();
				fos.close();
			} else {
				System.out.println("Ya existeix aquest Mokepon");
			}
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		}
	}
	
	public static boolean isMokepon(MokeponCapturat mok) {
		try {
			File f = new File("objectes.dat");
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Object obj = ois.readObject();
				if (obj instanceof MokeponCapturat)
					if (mok.equals((MokeponCapturat) obj)) {
						ois.close();
						return true;
					}
			}
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
		return false;
	}
	
	
	public static MokeponCapturat recuperarMokepon(String nom, int nivell, String entrenador, String nomDonat) {
		FileInputStream fis=null;
		ObjectInputStream ois = null;

		FileOutputStream fos=null;
		AppendableObjectOutputStream aoos = null;
		
		try {
			File original = new File("objectes.dat");
			fis = new FileInputStream(original);
			ois = new ObjectInputStream(fis);

			File temporal = new File("temporal.dat");
			fos = new FileOutputStream(temporal);
			aoos = new AppendableObjectOutputStream(fos, true);

			boolean trobat = false;
			while (true) {
				Object obj = ois.readObject();
				if (obj instanceof MokeponCapturat) {
					MokeponCapturat mok = ((MokeponCapturat) obj);
					if (!(mok.getNom().equals(nom) && mok.getNivell() == nivell && mok.getNomEntrenador().equals(entrenador) && mok.getNomPosat().equals(nomDonat))) {
						aoos.writeObject(mok);
					} else {
						if (!trobat) {
							trobat = true;
						} else {
							aoos.writeObject(mok);
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			try {
				aoos.close();
				File original = new File("objectes.dat");
				fis = new FileInputStream(original);
				ois = new ObjectInputStream(fis);

				File temporal = new File("temporal.dat");
				while (true) {
					Object obj = ois.readObject();
					if (obj instanceof MokeponCapturat) {
						MokeponCapturat mok = ((MokeponCapturat) obj);
						if ((mok.getNom().equals(nom) && mok.getNivell() == nivell && mok.getNomEntrenador().equals(entrenador) && mok.getNomPosat().equals(nomDonat))) {
							ois.close();
							fis.close();
							
							if (original.delete()) System.out.println("\nEl fichero ha sido borrado satisfactoriamente");
							else System.out.println("\nEl fichero no puede ser borrado");
							
					        if (temporal.renameTo(original)) System.out.println("\nArchivo renombrado");
					        else System.out.println("\nError");

							return mok;
						}
					}
				}
			} catch (FileNotFoundException x) {
				System.out.println("No existeix aquest fitxer");
				e.printStackTrace();
			} catch (IOException x) {
				System.out.println();
			} catch (ClassNotFoundException x) {
				System.out.println("No s'ha trobat la Classe");
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static void teamMocketAtacDeNou() {
		FileInputStream fis=null;
		ObjectInputStream ois = null;

		FileOutputStream fos=null;
		AppendableObjectOutputStream aoos = null;
		try {
			File original = new File("objectes.dat");
			fis = new FileInputStream(original);
			ois = new ObjectInputStream(fis);

			File temporal = new File("temporal.dat");
			fos = new FileOutputStream(temporal);
			aoos = new AppendableObjectOutputStream(fos, true);
			
			while (true) {
				Object obj = ois.readObject();
				if(obj instanceof MokeponCapturat) {
					MokeponCapturat mok = (MokeponCapturat)obj;
					mok.setNomEntrenador("Team Mocket");
					aoos.writeObject(mok);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("No existeix aquest fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			try {
				aoos.close();
				File original = new File("objectes.dat");
				File temporal = new File("temporal.dat");
				ois.close();
				fis.close();
				
				if (original.delete()) System.out.println("\nEl fichero ha sido borrado satisfactoriamente");
				else System.out.println("\nEl fichero no puede ser borrado");
				
		        if (temporal.renameTo(original)) System.out.println("\nArchivo renombrado");
		        else System.out.println("\nError");
		        
			} catch (FileNotFoundException x) {
				System.out.println("No existeix aquest fitxer");
				e.printStackTrace();
			} catch (IOException x) {
			}
		}catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat la Classe");
			e.printStackTrace();
		}
	}


}
