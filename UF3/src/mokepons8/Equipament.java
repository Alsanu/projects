package mokepons8;

public interface Equipament {

	abstract public void equipar(MokeponCapturat m);
	
	abstract public void desequipar(MokeponCapturat m);
	
	public default boolean potEquipar(MokeponCapturat m) {
		if (m.getEquip() != null || !m.isDebilitat()) return false;
		return false;
	}
	
	public default boolean malEquipat(MokeponCapturat m) {
		if (m.getObj() == null) return false;
		if (m.getObj() instanceof Equipament) return true;
		return false;
	}
}
