package mokepons8;

public class Armadura extends Objecte implements Equipament{
	
	private static final long serialVersionUID = 1L;
	
	private int defExtra;

	public Armadura(String nom) {
		super(nom);
	}

	@Override
	public void equipar(MokeponCapturat m) {
		m.setEquip(this);
		m.setDef(m.getDef() + this.defExtra);
	}

	@Override
	public void desequipar(MokeponCapturat m) {
		m.setEquip(null);
		m.setDef(m.getDef() - this.defExtra);
	}

	@Override
	public void utilitzar(MokeponCapturat m) {
		equipar(m);
	}

	public int getDefExtra() {
		return defExtra;
	}public void setDefExtra(int defExtra) {
		this.defExtra = defExtra;
	}
}
