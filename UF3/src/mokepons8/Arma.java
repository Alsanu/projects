package mokepons8;

public class Arma extends Objecte implements Equipament {
	
	private static final long serialVersionUID = 1L;
	
	private int atacExtra;

	public Arma(String nom) {
		super(nom);
	}

	@Override
	public void utilitzar(MokeponCapturat m) {
		equipar(m);
	}

	@Override
	public void equipar(MokeponCapturat m) {
		m.setEquip(this);
		m.setAtk(m.getAtk() + this.atacExtra);
	}

	@Override
	public void desequipar(MokeponCapturat m) {
		m.setEquip(null);
		m.setAtk(m.getAtk() - this.atacExtra);
	}
	
	public int getAtacExtra() {
		return atacExtra;
	}public void setAtacExtra(int atacExtra) {
		this.atacExtra = atacExtra;
	}
}
