package mokepons8;

import java.util.Random;
import java.util.Scanner;

public class Combat {
	
	static Scanner sc = new Scanner(System.in);
	static Random rm = new Random();
	
	public static void combate(Mokepon kenu, Mokepon m) {
		System.out.println("THE FIGHT BETWEEN " + kenu.getNom() + " AND " + m.getNom() + " BEGINS \n");
		boolean fi = false;
		int torn = MokeponMesRapid(kenu, m);
		while(!fi) {
			info(kenu, m, torn);
			if (torn == 1) { int numAtac = MostraAtacs(kenu); kenu.atacar(m, numAtac); torn = 2;
			} else { int numAtac = MostraAtacs(m); m.atacar(kenu, numAtac); torn = 1;}
			fi = fiCombat(kenu, m);
		}
	}
	
	public static int MokeponMesRapid(Mokepon mc, Mokepon m) {
		if (mc.getVel() >= m.getVel()) return 1;
		return 2;
	}
	
	public static void info(Mokepon mc, Mokepon m, int torn) {
		System.out.println("Name: " + m.getNom() + "         Hp: " + m.getHp_actual());
		System.out.println("\n \n ");
		System.out.println("Name: " + mc.getNom() + "         Hp: " + mc.getHp_actual() + "\n");
		if (torn == 1) System.out.println("Turn of " + mc.getNom());
		else System.out.println("Turn of " + m.getNom());
	}
	
	public static boolean fiCombat(Mokepon mc, Mokepon m) {
		if (mc.isDebilitat()) {
			System.out.println(mc.getNom() + " ha sigut debilitat per " + m.getNom());
			return true;
		}
		if (m.isDebilitat()) {
			System.out.println(m.getNom() + " ha sigut debilitat per " + mc.getNom());
			return true;
		}
		return false;
	}
	
	public static int MostraAtacs(Mokepon m) {
		if (m instanceof MokeponCapturat) {
			System.out.println("Attack list: \n");
			int i = 0;
			for (Atacs at: m.getAtacs()) {
				i++;
				System.out.println(i +" - " + at);
			}
			System.out.print("Choose one attack: ");
			return ComprobarAtaque(sc.nextInt(), i);
		}else return rm.nextInt(1, (m.getAtacs().size()+1));
		
		
	}
	
	public static int ComprobarAtaque(int num, int numA) {
		boolean valid = false;
		while (!valid) {
			if (num > numA || num < 1) {
				System.out.println("The attack that you chosed is not valid");
				System.out.print("Choose a valid attack: ");
				num = sc.nextInt();
			}else valid = true;
		}
		return num;
	}
}
