package mokepons7;

public class Reviure extends Objecte{

	public Reviure(String nom) {
		super(nom);
		// TODO Auto-generated constructor stub
	}

	Reviure(String nom, int quantitat){
		this(nom);
		this.quantitat = quantitat;
	}

	@Override
	public void utilitzar(MokeponCapturat m) {
		if (this.getQuantitat()<1) return;
		if (m.isDebilitat()) m.setDebilitat(false);
		this.quantitat--;
	}

}
