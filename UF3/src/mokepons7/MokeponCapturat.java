package mokepons7;

import java.util.Objects;

public class MokeponCapturat extends Mokepon{
	private String nomPosat;
	private String nomEntrenador;
	private int felicitat;
	private static int NumMokepons;
	private Objecte obj;
	private Equipament equip;

	public MokeponCapturat() {
		super();
		this.nomPosat = "Sense definir";
		this.nomEntrenador = "Sense definir";
		this.felicitat = 10;
		NumMokepons++;
	}
	
	public MokeponCapturat(String nom) {
		super(nom);
		this.nomPosat = nom;
		this.nomEntrenador = "Josevi";
		this.felicitat = 10;
		NumMokepons++;
	}
	
	public MokeponCapturat(String nom, Sexe sexe) {
		super(nom, sexe);
		this.nomPosat = nom;
		this.nomEntrenador = "Josevi";
		this.felicitat = 10;
		NumMokepons++;
	}

	public MokeponCapturat(String nom, Tipus tipus) {
		super(nom, tipus);
		this.nomPosat = nom;
		this.nomEntrenador = "Josevi";
		this.felicitat = 10;
		NumMokepons++;
	}
	
	public MokeponCapturat(String nom, int nivell) {
		super(nom, nivell);
		this.nomPosat = nom;
		this.nomEntrenador = "Josevi";
		this.felicitat = 10;
		NumMokepons++;
	}
	
	public MokeponCapturat(String nom, int nivell, int hp_max, int atk, int def, int vel, Tipus tipus) {
		super(nom, nivell, hp_max, atk, def, vel, tipus);
		this.nomPosat = nom;
		this.nomEntrenador = "Josevi";
		this.felicitat = 10;
		NumMokepons++;
	}
	
	public MokeponCapturat(Mokepon MK, String nomPosat, String nomEntrenador) {
		this.setNivell(MK.getNivell());
		this.setNom(MK.getNom());
		this.setAtk(MK.getAtk());
		this.setDebilitat(MK.isDebilitat());
		this.setDef(MK.getDef());
		this.setMax_exp(MK.getMax_exp());
		this.setVel(MK.getVel());
		this.felicitat = 50;
		this.setHp_actual(MK.getMax_exp());
		this.setHp_max(MK.getHp_max());
		this.nomEntrenador = nomEntrenador;
		this.nomPosat = nomPosat;
		this.setTipus(MK.getTipus());
		this.setAtacs(MK.getAtacs());
	}
	
	public Ou reproduccio(MokeponCapturat Parella) throws Exception{
		if (this.getTipus() == Parella.getTipus()) {
			if (this.getSexe() != Parella.getSexe()) {
				if (!this.isDebilitat() && !Parella.isDebilitat()) {
					int sn = rm.nextInt(0,2);
					if (sn == 0) return (new Ou(this.getNom(), this.getTipus()));
					return (new Ou(Parella.getNom(), this.getTipus()));
				}
				throw new DebilitatException("Error, un dels mokpons esta debilitat");
			}
			throw new SexeIgualException("Error, son del mateix sexe");
		}
		throw new TipusDiferentException("Error, son de diferent tipus");
	}

	public void acariciar() {
		this.felicitat += 10;
		if (this.felicitat > 100) this.felicitat = 100;
	}
	
	public void atacar(Mokepon atacat , int num_atac) {
		if (this.isDebilitat()) System.out.println(this.getNom() + " esta debilitat i no pot atacar");
		else {
			Atacs at = this.getAtacs().get(num_atac-1);
			double bonus = super.efectivitat(at.tipus, atacat.getTipus());
			double damage = ((((2 * this.getNivell() ) / 5 + 2 ) * at.poder * (this.getAtk() / atacat.getDef())) / 50 + 2) * bonus;
			if (this.felicitat >= 50) damage *= 1.2;
			else damage *= 0.8;
			atacat.setHp_actual(atacat.getHp_actual() -  (int) damage);
			debilitarse(atacat);
		}
	}
	
	public void utilitzarObjecte() {
		if (this.obj != null) this.obj.utilitzar(this);
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "================================ \n";
		cadena += "Nom: " + this.nomPosat + "\n";
		cadena += "Entrenador: " + this.nomEntrenador + "\n";
		cadena += "Felicitat: " + this.felicitat + "\n";
		return cadena;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(equip, felicitat, nomEntrenador, nomPosat, obj);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
	if (this == obj) return true;
	if (obj == null) return false;
	if (getClass() != obj.getClass()) return false;
	Mokepon ot = (Mokepon) obj;
	if (this.getNom().equals(ot.getNom()) && this.getSexe() == ot.getSexe() && this.getNivell() == ot.getNivell() && this.getTipus() == ot.getTipus()
	&& this.getAtk() == ot.getAtk() && this.getDef() == ot.getDef() && this.getVel() == ot.getVel() && this.getHp_max() == ot.getHp_max())
	return true;

	else return false;

	}
	
	public String getNomEntrenador() {
		return nomEntrenador;
	}public void setNomEntrenador(String nomEntrenador) {
		this.nomEntrenador = nomEntrenador;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}
	
	public Equipament getEquip() {
		return equip;
	}public void setEquip(Equipament equip) {
		this.equip = equip;
	}

	public Objecte getObj() {
		return obj;
	}public void setObj(Objecte obj) {
		this.obj = obj;
	}
	
	public String getNomPosat() {
		return nomPosat;
	}public void setNomPosat(String nomPosat) {
		this.nomPosat = nomPosat;
	}

	public static int getNumMokepons() {
		return NumMokepons;
	}public static void setNumMokepons(int numMokepons) {
		NumMokepons = numMokepons;
	}
}
