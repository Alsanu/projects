package mokepons7;

public class Test {

	public static void main(String[] args) throws Exception {
		Mokepon Kenu = new Mokepon("Keni", 30, 100, 30, 30, 30, Tipus.GRASS);
		Atacs Queso = new Atacs("Que-so", 80, Tipus.GRASS, 10);
		Kenu.afegirAtac(Queso);
		Atacs Follaje = new Atacs("Follaje", 40, Tipus.GRASS, 20);
		Kenu.afegirAtac(Follaje);
		System.out.println(Kenu);
		Kenu = Kenu.capturar("Josevi", "Keni");
		System.out.println(Kenu);

		Mokepon Juanjo = new Mokepon("Jom", 20, 80, 20, 20, 20, Tipus.WATER);
		Atacs QuackWalk = new Atacs("Quack-Walk", 60, Tipus.WATER, 15);
		Juanjo.afegirAtac(QuackWalk);
		Atacs Gapo = new Atacs("Gapo", 30, Tipus.WATER, 25);
		Juanjo.afegirAtac(Gapo);
		
		Combat.combate(Kenu, Juanjo);
	}

}
