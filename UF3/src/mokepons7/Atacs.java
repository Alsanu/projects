package mokepons7;

public class Atacs {

	private String nomA;
	double poder;
	Tipus tipus;
	int mov_max;
	int mov_act;
	
	public Atacs(String nom, double poder, Tipus tipus, int mov_max) {
		this.nomA = nom;
		this.poder = poder;
		if (poder > 100) this.poder = 100;
		else if (poder < 10) this.poder = 10;
		this.tipus = tipus;
		this.mov_max = mov_max;
		this.mov_act = this.mov_max;
	}
	
	public Atacs(String nom, Tipus tipus) {
		this.nomA = nom;
		this.tipus = tipus;
		this.poder = 10;
		this.mov_max = 10;
		this.mov_act = this.mov_max;
	}
	
	@Override
	public String toString() {
		String cadena = "Name: " + this.nomA + "\n";
		cadena += " - Power: " + this.poder + "\n";
		cadena += " - Type: " + this.tipus + "\n";
		cadena += " - Max PP: " + this.mov_max + "\n";
		cadena += " - Current PP: " + this.mov_act + "\n";
		return cadena;
	}
	
	public String getNomA() {
		return nomA;
	}public void setNomA(String nom) {
		this.nomA = nom;
	}
	
}
