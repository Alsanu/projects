package mokepons7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Fitxers {
	
	public static void afegirGimnas(String nomGimnas, String ciutat, String liderGimnas, int numGuanyadors) throws Exception{
		try {
			File Gimnas = new File("temporal.txt");
			FileWriter fw = new FileWriter(Gimnas, true);
			BufferedWriter bw = new BufferedWriter(fw);

			bw.append(nomGimnas+";"+ ciutat +";"+ liderGimnas +";"+ numGuanyadors +"\n");
			
			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void mostraGimnasos() {
		try {
			File f = new File("gimnassos.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String Gimnas;
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				String cadena = "\n================================================\n";
				cadena += "Gimnas: " + GimnasParts[0] + "\n";
				cadena += "Ciutat: " + GimnasParts[1] + "\n";
				cadena += "Lider: " + GimnasParts[2] + "\n";
				cadena += "Entrenadors que han superat aquest Gimnas: " + GimnasParts[3] + "\n";
				cadena += "================================================";
				System.out.println(cadena);
			}
			
			br.close();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void cercaLider(String nomGimnas) {
		try {
			File f = new File("gimnassos.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String Gimnas; String cadena = null;
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				if (GimnasParts[0].equals(nomGimnas)) {
					cadena = "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
					cadena += "Gimnas: " + GimnasParts[0] + "\n";
					cadena += "Lider: " + GimnasParts[2] + "\n";
					cadena += "++++++++++++++++++++++++++++++++++++++++++++++++";
					System.out.println(cadena);
				}
			}
			if (cadena == null) System.out.println("Gimnas no trobat");
			
			br.close();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void invictes(int n) {
		try {
			File f = new File("gimnassos.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String Gimnas;
			String cadena = "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				int num = Integer.parseInt(GimnasParts[3]);
				if (num > n) {
					cadena += GimnasParts[0] + " ha sigut vençut " + num + " vegades\n";
				}
			}
			cadena += "++++++++++++++++++++++++++++++++++++++++++++++++";
			System.out.println(cadena);
			
			br.close();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void copiaSeguretat(String path1, String path2) {
		try {
			File origen = new File(path1);
			File destino = new File(path2);
			
			Files.copy(origen.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void canviLider(String nomGimnas, String nouLider) {
		try {
			File origen = new File("gimnassos.txt");
			FileReader fr = new FileReader(origen);
			BufferedReader br = new BufferedReader(fr);
			
			File destino = new File("temporal.txt");
			FileWriter fw = new FileWriter(destino, true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String Gimnas;
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				if (nomGimnas.equals(GimnasParts[0])) {
					GimnasParts[2] = nouLider;
				}
				Gimnas = "";
				for (int i = 0; i < GimnasParts.length; i++) {
					Gimnas += GimnasParts[i];
					if (i == GimnasParts.length-1) Gimnas += "\n";
					else Gimnas += ";";
				}
				bw.append(Gimnas);
			}
			
			bw.flush();
			bw.close();
			br.close();
			
			if (origen.delete()) System.out.println("\nEl fichero ha sido borrado satisfactoriamente");
			else System.out.println("\nEl fichero no puede ser borrado");
			
	        if (destino.renameTo(origen)) System.out.println("\nArchivo renombrado");
	        else System.out.println("\nError");
			
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void afegirEntrenador(String nomGimnas, String entrenador) {
		try {
			File origen = new File("gimnassos.txt");
			FileReader fr = new FileReader(origen);
			BufferedReader br = new BufferedReader(fr);
			
			File destino = new File("temporal");
			FileWriter fw = new FileWriter(destino, true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String Gimnas;
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				if (nomGimnas.equals(GimnasParts[0])) {
					int num = Integer.parseInt(GimnasParts[3])+1;
					GimnasParts[3] = String.valueOf(num);
				}
				Gimnas = "";
				for (int i = 0; i < GimnasParts.length; i++) {
					Gimnas += GimnasParts[i] + ";";
				}
				if (nomGimnas.equals(GimnasParts[0])) {
					Gimnas += entrenador;
				}
				Gimnas += "\n";
				bw.append(Gimnas);
			}
			
			bw.flush();
			bw.close();
			br.close();
			
			if (origen.delete()) System.out.println("\nEl fichero ha sido borrado satisfactoriamente");
			else System.out.println("\nEl fichero no puede ser borrado");
			
	        if (destino.renameTo(origen)) System.out.println("\nArchivo renombrado");
	        else System.out.println("\nError");
			
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void esborraGimnas(String nomGimnas) {
		try {
			File origen = new File("gimnassos.txt");
			FileReader fr = new FileReader(origen);
			BufferedReader br = new BufferedReader(fr);
			
			File destino = new File("temporal");
			FileWriter fw = new FileWriter(destino, true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String Gimnas;
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				if (!nomGimnas.equals(GimnasParts[0])) {
					Gimnas = "";
					for (int i = 0; i < GimnasParts.length; i++) {
						Gimnas += GimnasParts[i];
						if (i == GimnasParts.length-1) Gimnas += "\n";
						else Gimnas += ";";
					}
					bw.append(Gimnas);
				}
			}
			
			bw.flush();
			bw.close();
			br.close();
			
			if (origen.delete()) System.out.println("\nEl fichero ha sido borrado satisfactoriamente");
			else System.out.println("\nEl fichero no puede ser borrado");
			
	        if (destino.renameTo(origen)) System.out.println("\nArchivo renombrado");
	        else System.out.println("\nError");
			
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
	
	public static void consultaEntrenadors(String nomGimnas) {
		try {
			File f = new File("gimnassos.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			String Gimnas; 
			String cadena = "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
			while ((Gimnas = br.readLine()) != null) {
				String[] GimnasParts = Gimnas.split(";");
				if (GimnasParts[0].equals(nomGimnas)) {
					String entrenadors = "";
					for (int i = 4; i < GimnasParts.length; i++) {
						entrenadors += GimnasParts[i] + "  ";
					}
					cadena += GimnasParts[0] + " ha sigut vençut per: " + entrenadors + "\n";
				}
			}
			cadena += "++++++++++++++++++++++++++++++++++++++++++++++++";
			System.out.println(cadena);
			
			br.close();
		} catch (IOException e) {
			System.out.println("Excepció general d'escriptura");
            e.printStackTrace();
		}
	}
}
