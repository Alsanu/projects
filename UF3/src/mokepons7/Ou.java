package mokepons7;

import java.util.Random;

public class Ou {
	Random rm = new Random();
	
	private String especie;
	private Tipus tipus;
	private int passesRestants;
	
	public Ou(String especie, Tipus tipus) {
		this.especie = especie;
		this.tipus = tipus;
		this.passesRestants = rm.nextInt(5,11);
	}
	
	public void caminar() {
		this.passesRestants--;
		MokeponCapturat m;
		if (passesRestants == 0) m = eclosionar();
	}
	
	public MokeponCapturat eclosionar() {
		MokeponCapturat m = new MokeponCapturat(this.especie, this.tipus);
		return m;
	}

}
