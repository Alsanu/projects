package Escacs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Aquesta classe serveix per veure tots els possibles moviments de les peces del joc escacs a partir d'una casella que dona l'usuari.
 * @author Alberto Sanchez
 *
 */
public class Moviments {

	/**
	 * Mètode que permet trucar a tots els altres mètodes de la classe i escriu per pantalla totes les posicions disponibles de la pesa que dona l'usuari.
	 * @param pesa - valor ingressat per l'usuari que s'utilitza per conèixer quina pesa ha de comprovar el programa.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> superfuncio(char pesa, int casella) {
		ArrayList<Integer> moviments = superfuncio(pesa, casella);
		switch (pesa) {
		case 'C': moviments = cavall(casella);
		case 'T': moviments = torre(casella);
		case 'A': moviments = alfil(casella);
		case 'P': moviments = peo(casella);
		case 'D': moviments = dama(casella);
		case 'R': moviments = rei(casella);
		}
		Collections.sort(moviments);
		System.out.print("Posicions disponibles: ");
		int cont=0;
		for (int i: moviments) {
			cont++;
			if (cont == moviments.size()) {
				System.out.print(i);
			}else {
				System.out.print(i + ",");
			}
		}
		System.out.println();
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure el cavall a la posicio que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> cavall(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		int fila = casella / 10; int columna = casella % 10; 
		if (fila-1 >= 1 && columna-2 >= 1) {
			moviments.add((fila-1) * 10 + (columna-2));
		}if (fila-2 >= 1 && columna-1 >= 1) {
			moviments.add((fila-2) * 10 + (columna-1));
		}if (fila+1 <= 8 && columna+2 <= 8) {
			moviments.add((fila+1) * 10 + (columna+2));
		}if (fila+2 <= 8 && columna+1 <= 8) {
			moviments.add((fila+2) * 10 + (columna+1));
		}if (fila-1 >= 1 && columna+2 <= 8) {
			moviments.add((fila-1) * 10 + (columna+2));
		}if (fila-2 >= 1 && columna+1 <= 8) {
			moviments.add((fila-2) * 10 + (columna+1));
		}if (fila+1 <= 8 && columna-2 >= 1) {
			moviments.add((fila+1) * 10 + (columna-2));
		}if (fila+2 <= 8 && columna-1 >= 1) {
			moviments.add((fila+2) * 10 + (columna-1));
		}
		
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure la torre a la posicio que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> torre(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		int fila = casella / 10; int columna = casella % 10;
		
		for (int i = 1; i <= 8; i++) {
			if (i != columna) {
				moviments.add(fila * 10 + i);
			}
		}
		
		for (int i = 1; i <= 8; i++) {
			if (i != fila) {
				moviments.add(i * 10 + columna);
			}
		}
		
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure l'alfil a la posicio que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> alfil(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		int fila = casella / 10; int columna = casella % 10;
		int aux1 = fila; int aux2 = columna;
		boolean dins = true;
		while (dins) {
			aux1--; aux2--;
			if (aux1 >= 1 && aux1 <= 8 && aux2 >= 1 && aux2 <= 8) {
				moviments.add(aux1 * 10 + aux2);
			}else {
				dins = false;
			}
		}
		dins = true;
		aux1 = fila; aux2 = columna;
		while (dins) {
			aux1++; aux2++;
			if (aux1 >= 1 && aux1 <= 8 && aux2 >= 1 && aux2 <= 8) {
				moviments.add(aux1 * 10 + aux2);
			}else {
				dins = false;
			}
		}
		dins = true;
		aux1 = fila; aux2 = columna;
		while (dins) {
			aux1++; aux2--;
			if (aux1 >= 1 && aux1 <= 8 && aux2 >= 1 && aux2 <= 8) {
				moviments.add(aux1 * 10 + aux2);
			}else {
				dins = false;
			}
		}
		dins = true;
		aux1 = fila; aux2 = columna;
		while (dins) {
			aux1--; aux2++;
			if (aux1 >= 1 && aux1 <= 8 && aux2 >= 1 && aux2 <= 8) {
				moviments.add(aux1 * 10 + aux2);
			}else {
				dins = false;
			}
		}
		
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure el peo a la posicio que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> peo(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		int fila = casella / 10; int columna = casella % 10;
		if (fila > 1) {
			moviments.add((fila - 1) * 10 + columna);
			if (fila == 7) {
				moviments.add((fila - 2) * 10 + columna);
			}
		}
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure la torre a la dama que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> dama(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		moviments.addAll(torre(casella));
		moviments.addAll(alfil(casella));
		return moviments;
	}
	
	/**
	 * Mètode que serveix per coneixer totes les posicions a les que es pot moure el rei a la posicio que dona l'usuari.
	 * @param casella - valor ingressat per l'usuari que s'utilitza per conèixer la posició inicial de la pesa.
	 * @return - retorna un array amb totes les posicions disponibles a les que es pot moure la pesa.
	 */
	public static ArrayList<Integer> rei(int casella) {
		ArrayList<Integer> moviments = new ArrayList<Integer>();
		int fila = casella / 10; int columna = casella % 10;
		for (int i = fila-1; i < fila+2; i++) {
			for (int x = columna-1; x < columna+2; x++) {
				if (i >= 1 && i <= 8 && x >= 1 && x <= 8) {
					if ((i * 10) + x != casella) {
						moviments.add(i * 10 + x);
					}
				}
			}
		}
		return moviments;
	}
}
