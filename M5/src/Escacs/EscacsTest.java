package Escacs;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class EscacsTest {

	@Test
	public void test() {
		// TORRES
		assertEquals(new ArrayList<>(Arrays.asList(12,13,14,15,16,17,18,21,31,41,51,61,71,81)), Moviments.superfuncio('T', 11));
		assertEquals(new ArrayList<>(Arrays.asList(15,25,35,45,51,52,53,54,56,57,58,65,75,85)), Moviments.superfuncio('T', 55));
		assertEquals(new ArrayList<>(Arrays.asList(18,28,38,48,58,61,62,63,64,65,66,67,78,88)), Moviments.superfuncio('T', 68));
		assertEquals(new ArrayList<>(Arrays.asList(15,21,22,23,24,26,27,28,35,45,55,65,75,85)), Moviments.superfuncio('T', 25));
		
		//PEONES
		assertEquals(new ArrayList<>(Arrays.asList(54,64)), Moviments.superfuncio('P', 74));
		assertEquals(new ArrayList<>(Arrays.asList(34)), Moviments.superfuncio('P', 44));
		assertEquals(new ArrayList<>(Arrays.asList()), Moviments.superfuncio('P', 14));
		assertEquals(new ArrayList<>(Arrays.asList(71)), Moviments.superfuncio('P', 81));
		
		//REI
		assertEquals(new ArrayList<>(Arrays.asList(77,78,87)), Moviments.superfuncio('R', 88));
		assertEquals(new ArrayList<>(Arrays.asList(31,32,42,51,52)), Moviments.superfuncio('R', 41));
		assertEquals(new ArrayList<>(Arrays.asList(24,25,26,34,36,44,45,46)), Moviments.superfuncio('R', 35));
		assertEquals(new ArrayList<>(Arrays.asList(16,18,26,27,28)), Moviments.superfuncio('R', 17));
		
		//DAMA
		assertEquals(new ArrayList<>(Arrays.asList(13,17,24,27,35,37,46,47,48,51,52,53,54,55,56,58,66,67,68,75,77,84,87)), Moviments.superfuncio('D', 57));
		assertEquals(new ArrayList<>(Arrays.asList(11,12,13,14,15,16,17,27,28,36,38,45,48,54,58,63,68,72,78,81,88)), Moviments.superfuncio('D', 18));
		assertEquals(new ArrayList<>(Arrays.asList(13,23,33,38,43,47,53,56,61,63,65,72,73,74,81,82,84,85,86,87,88)), Moviments.superfuncio('D', 83));
		assertEquals(new ArrayList<>(Arrays.asList(11,14,21,23,31,32,42,43,44,45,46,47,48,51,52,61,63,71,74,81,85)), Moviments.superfuncio('D', 41));
		
		//ALFILES
		assertEquals(new ArrayList<>(Arrays.asList(18,27,36,45,54,63,72)), Moviments.superfuncio('A', 81));
		assertEquals(new ArrayList<>(Arrays.asList(12,16,23,25,43,45,52,56,61,67,78)), Moviments.superfuncio('A', 34));
		assertEquals(new ArrayList<>(Arrays.asList(17,37,46,55,64,73,82)), Moviments.superfuncio('A', 28));
		assertEquals(new ArrayList<>(Arrays.asList(22,24,31,35,46,57,68)), Moviments.superfuncio('A', 13));
		
		//CABALLOS
		assertEquals(new ArrayList<>(Arrays.asList(62,73)), Moviments.superfuncio('C', 81));
		assertEquals(new ArrayList<>(Arrays.asList(17,26,46,57)), Moviments.superfuncio('C', 38));
		assertEquals(new ArrayList<>(Arrays.asList(25,36,38)), Moviments.superfuncio('C', 17));
		assertEquals(new ArrayList<>(Arrays.asList(52,54,61,65,81,85)), Moviments.superfuncio('C', 73));
		assertEquals(new ArrayList<>(Arrays.asList(52,54,61,65,81,85)), Moviments.superfuncio('C', 66));
		
	}

}
