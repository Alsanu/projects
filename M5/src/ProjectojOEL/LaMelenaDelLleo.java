package ProjectojOEL;

import java.util.ArrayList;
import java.util.Scanner;

public class LaMelenaDelLleo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int j = 0; j < casos; j++) {
			int fc = sc.nextInt();
			int[][] matrix = new int[fc][fc];
			for (int i = 0; i < fc; i++) {
				for (int x = 0; x < fc; x++) {
					matrix[i][x] = sc.nextInt();
				}
				System.out.println();
			}
			ArrayList<Integer> datosMelena = datosFinal(fc, matrix);
			for (int i = 0; i < fc; i++) {
				for (int x = 0; x < fc; x++) {
					System.out.print(matrix[i][x] + " ");
				}
				System.out.println();
			}
			System.out.println("Melena Pintada: " + datosMelena.get(0)); 
			System.out.println("Melena Esborrada: " + datosMelena.get(1));
			System.out.println("Gruix de la Melena: " + datosMelena.get(2));
		}
		sc.close();
	}
	
	public static ArrayList<Integer> datosFinal(int fc, int[][] matrix) {
		ArrayList<Integer> datosMelena = new ArrayList<Integer>();
		datosMelena.addAll(rellenar(fc, matrix));
		return datosMelena;
	}
	
	public static ArrayList<Integer> rellenar(int fc, int[][] matrix) {
		ArrayList<Integer> datosMelena = new ArrayList<Integer>();
		int PAfegits = 0, PCortats = 0, aux = 3, grosor = 0;
		for (int j = 0; j < fc; j++) {
			for (int k = 0; k < fc; k++) {
				int jaux = j; int kaux = k;
				if (matrix[j][k] == 2) {
					boolean salir = false;
					while (!salir) {
						jaux--; kaux--;
						boolean accion = comprovar(fc, matrix, jaux, kaux, aux); // (true = rellenar)(false = cortar)
						if (accion) {
							grosor++;
							for (int i = jaux; i < jaux+aux; i++) {
								for (int x = kaux; x < kaux+aux; x++) {
									if (matrix[i][x] == 0) {
										matrix[i][x] = 1;
										PAfegits++;
									}else {
									}
								}
							}
						}else {
							salir = true;
							for (int i = 0; i < fc; i++) {
								for (int x = 0; x < fc; x++) {
									if (i <= jaux || x <= kaux || i >= jaux+(aux-1) || x >= kaux+(aux-1)) {
										if (matrix[i][x] == 1) {
											matrix[i][x] = 0;
											PCortats++;
										}
									}
								}
							}
						}
						aux++;
						aux++;
					}
				}
			}
		}
		datosMelena.add(PAfegits); datosMelena.add(PCortats); datosMelena.add(grosor);
		return datosMelena;
	}
	
	public static boolean comprovar(int fc, int[][] matrix, int j, int k, int aux) {
		int uno = 0, zero = 0;
		for (int i = j; i < j+aux; i++) {
			for (int x = k; x < k+aux; x++) {
				if (i <= j || x <= k || i >= j+(aux-1) || x >= k+(aux-1)) {
					if (i < 0 || i >= fc || x < 0 || x >= fc) return false;
					if (matrix[i][x] == 1) uno++;
					else if (matrix[i][x] == 0) zero++;
				}
			}
		}
		if (uno >= zero) return true;
		else return false;
	}
	
}
